/*
 * Francesco Romani 20050324 - 15:00
 * albero di simboli
 */

#ifndef _SYMTREE_H_INCLUDED_
#define _SYMTREE_H_INCLUDED_

#include "common.h"

#define SYMTREE_VERSION		"0.3"

typedef uint16_t ptr_t;
/* 32 bit sarebbero meglio, ma forse troppo */

#define SYMTREE_MAX_NODES       ((ptr_t)65530)
#define SYMTREE_NULL            ((ptr_t)0)
#define SYMTREE_PTR_ROOT        ((ptr_t)1)
#define SYMTREE_PTR_FIRST       (SYMTREE_PTR_ROOT + 1)

#define SYMTREE_DOWNSTREAM      (0)
/** puntatori ai figli, ma non al padre */
#define SYMTREE_UPSTREAM        (1)
/** puntatore al padre, ma non ai figli */

struct _symtree_node_t {
	/** il simbolo ospitato nel nodo */
	sym_t sym;
	
	/**
	 * DOWNSTREAM : puntatore INDIRETTO alla catena di figli
	 * UPSTREAM   : puntatore DIRETTO al nodo genitore
	 */
	ptr_t ptr;
	
#ifdef LZ78_PROFILE	
	/**
	 * distanza tra il nodo (compreso) 
	 * e la radice (esclusa) 
	 */
	int dist;
#endif
};

typedef struct _symtree_node_t symtree_node_t;

/** blocco puntatore, utilizzato in alberi DOWNSTREAM */
struct _symtree_child_t {
	/** puntatore al prossimo blocco */
	ptr_t next;
	/** puntatore al NODO figlio */
	ptr_t son;
};

typedef struct _symtree_child_t symtree_child_t;

struct _symtree_t {
	/** UPSTREAM o DOWNSTREAM? */
	int mode;
	
	/**
	 * numero di nodi _e_ blocchi puntatori 
	 * effettivamente presente
	 */
	int size;

	/** array di blocchi per nodi e puntatori */
	symtree_node_t *nodes;	
	symtree_child_t *ptrs;
	
	/**
	 * indice ultimo nodo: i nodi vengono
	 * allocati sequenzialmente, sebbene i 
	 * collegamenti tra loro non lo siano.
	 */
	ptr_t node_last;
	
	/**
	 * come sopra, ma per i blocchi puntatore
	 */
	ptr_t ptr_last;	
};

typedef struct _symtree_t symtree_t;

/**************************************************************************** 
 * funzioni esportate                                                       *
 ****************************************************************************/

/**
 * inizializza un nuovo albero, con le dimensioni e il tipo indicato
 *
 * @param tree		puntatore all'albero da inizializzare
 * @param size		dimensione (in nodi) dell'albero da inizializzare
 * @param mode		modo di funzionamento dell'albero: 
 * 			UPSTREAM o DOWNSTREAM
 * 			
 * @return		0 in caso di successo, !0 altrimenti
 * 
 * @see	symtree_fini
 * @see symtree_reset
 */
int symtree_init(symtree_t *tree, int size, int mode);

/**
 * finalizza un'albero, liberando resettandolo e liberando le risorse
 * acquisite.
 *
 * @param tree		puntatore all'albero da finalizzare
 * 
 * @see symtree_init
 * @see symtree_reset
 */
void symtree_fini(symtree_t *tree);

/**
 * reimposta lo stato iniziale, come subito dopo un _init avvenuta con 
 * successo, di un'albero.
 * NOTA: questo NON dealloca le risorse acquisite
 *
 * @param tree		puntatore all'albero da reimpostare
 * 
 * @see symtree_init
 * @see symtree_fini
 */
void symtree_reset(symtree_t *tree);


/**
 * reimposta lo stato iniziale come symtree_reset, ma solo se
 * i nodi o i blocchi puntatori sono esauriti o prossimi
 * all'esaurimento.
 *
 * @param tree		puntatore all'albero da reimpostare
 * 
 * @return		TRUE se e` avvenuto un reset, FALSE altrimenti
 * 
 * @see symtree_reset
 * @see symtree_init
 */
int symtree_reset_if_no_space(symtree_t *tree);

/**
 * [MACRO]
 * accede al simbolo contenuto nel nodo indicato
 *
 * @param tree		puntatore all'albero su cui operare
 * @param node		indice del node di cui prelevare il simbolo
 * 
 * @return		il simbolo del nodo indicato
 *
 * @see NODE_SET_SYM
 */
#define NODE_GET_SYM(tree, node)        ((tree)->nodes[(node)].sym)


/**
 * [MACRO]
 * imposta il simbolo del nodo indicato
 *
 * @param tree		puntatore all'albero su cui operare
 * @param node		indice del node di cui impostare il simbolo
 * 
 * @return		il nuovo simbolo impostato
 *
 */
#define NODE_SET_SYM(tree, node, s)	((tree)->nodes[(node)].sym = (s))

/**
 * Nota sui puntatori ai nodi:
 * In vari metodi, tra cui symtree_get_sym, symtree_has_child, symtree_walk,
 * e` richiesto di passare un puntatore al nodo su cui operare, o da cui
 * partire a operare.
 * Questo puntatore e` rappresentato dal tipo opaco ptr_t.
 * Nell'implementazione attuale i riferimenti ai nodi si ottengono
 * mediante le costanti speciali (SYMTREE_PTR_*) o mediante il valore di
 * ritorno di symtree_add_child. 
 * Questo copre i casi d'uso attuali di symtree.
 */ 

/**
 * preleva il simbolo associato ad un dato nodo
 * ATTENZIONE: la radice NON HA MAI UN SIMBOLO ASSOCIATO 
 *
 * @param tree		puntatore all'albero su cui operare
 * @param node		puntatore al nodo di cui si vuole il simbolo
 * 			associato
 * 			
 * @return il valore del simbolo
 * 
 * @see symtree_add_child
 * @seee symtree_init
 */
//questo fa contento gcc
//sym_t symtree_get_sym(symtree_t *tree, ptr_t node);
sym_t symtree_get_sym(symtree_t *tree, int node);

/**
 * aggiunge un figlio con il simbolo indicato al nodo indicato
 *
 * @param tree		puntatore all'albero su cui operare
 * @param node		puntatore al nodo cui aggiungere il figlio
 * @param sym		simbolo da associare al figlio
 *
 * @return		un puntatore al figlio inserito, o SYMTREE_NULL
 * 			se l'inserimento fallisce. Questo solitamente
 * 			indica che e` stato raggiunta la dimensione massima.
 * 			
 * @see symtree_init
 * @see symtree_get_sym
 */
//questo fa contento gcc
//ptr_t symtree_add_child(symtree_t *tree, ptr_t node, sym_t sym);
ptr_t symtree_add_child(symtree_t *tree, int node, int sym);

/**
 * verifica se il nodo indicato ha, tra i suoi figli *diretti*, 
 * uno cui e` associato il simbolo fornito
 * ATTENZIONE: questo metodo fallisce implicitamente su alberi di tipo
 *             UPSTREAM
 *
 * @param tree		puntatore all'albero su cui operare
 * @param node		puntatore al nodo i cui figli devono essere esaminati
 * 
 * @return		un puntatore al figlio cui e` associato il simbolo,
 * 			o SYMTREE_NULL se il nodo non ha figli o nessuno
 * 			dei figli ha associato il simbolo indicato
 * 			
 * @see symtree_init
 * @see symtree_get_sym
 * @see symtree_add_child
 */
//questo fa contento gcc
//ptr_t symtree_has_child(symtree_t *tree, ptr_t node, sym_t sym);
ptr_t symtree_has_child(symtree_t *tree, int node, int sym);

/**
 * ritorna un riferimento al nodo genitore non-radice di livello piu` alto
 * ('ancestor', per brevita`).
 * Nel caso di un nodo che e` figlio direttamente della radice, ritorna
 * un riferimento al nodo stesso.
 *
 * @param tree		puntatore all'albero su cui operare
 * @param node		puntatore al nodo di cui trovare il nodo 'ancestor'
 *
 * @return		un riferimento al nodo 'ancestor', o al nodo fornito
 * 			medesimo se l'unico genitore del nodo e` la radice
 * @see symtree_add_child
 * @see symtree_has_child
 */
//questo fa contento gcc
//ptr_t symtree_find_ancestor(symtree_t *tree, ptr_t node);
ptr_t symtree_find_ancestor(symtree_t *tree, int node);

/**
 * ritorna la distanza del nodo indicato dalla radice, compreso il nodo ma 
 * esclusa la radice medesima.
 * ATTENZIONE: la radice non viene contata perche` 
 *             NON HA MAI UN SIMBOLO ASSOCIATO
 * ATTENZIONE: questo metodo fallisce implicitamente su alberi di tipo
 *             DOWNSTREAM
 * 
 * esempio:
 * dato un albero, 'tree' cosi` composto:
 * [root]
 *      +-X
 *      +-X
 *      `-[parent] (2)
 *               +-X
 *               `-[nodeX] (1)
 * symtree_get_path_len(&tree, nodeX) -> 2
 *
 * @param tree		puntatore all'albero su cui operare
 * @param node		puntatore al nodo di cui calcolare la distanza 
 *                      dalla radice
 *
 * @return		la distanza del nodo, espressa in nodi, compreso
 *                      se stesso: minimo 1 quindi.
 *                      0 se node == SYSTREE_PTR_ROOT
 *                      -1 in caso di errori.
 *                      
 * @see systree_init
 * @see systree_add_child
 * @see systree_walk
 */
//questo fa contento gcc
//int symtree_get_path_len(symtree_t *tree, ptr_t node);
size_t symtree_get_path_len(symtree_t *tree, int node);

/**
 * crea il percorso di simboli a partire dalla radice (esclusa, vedi nota)
 * sino al nodo indicato, compreso. Memorizza il percorso di simboli
 * in un buffer fornito dal chiamante.
 * ATTENZIONE: la radice non viene considerata perche` 
 *             NON HA MAI UN SIMBOLO ASSOCIATO
 *             
 * @param tree		puntatore all'albero su cui operare
 * @param node		puntatore al nodo di destinazione 
 *                      (termine del percorso)
 * @param buf		buffer in cui memorizzare il percorso
 *                      di simboli
 * @param bufsize	dimensione del buffer.
 *
 * @return		>= 0 (la lunghezza del percorso) in caso di successo
 *                      -1 se il buffer non e` abbastanza capiente per
 *                      contenere l'intero percorso
 *                      
 * @see symtree_init
 * @see symtree_has_child
 * @see symtree_get_path_len
 */
//questo fa contento gcc
//int symtree_walk(symtree_t *tree, ptr_t node, sym_t *buf, int bufsize);
int symtree_walk(symtree_t *tree, int node, sym_t *buf, size_t bufsize);

/**
 * restituisce la quantita` di elementi, tra nodi e blocchi puntatore,
 * piu` prossima all'esaurimento
 *
 * @param tree		puntatore all'albero su cui operare
 * 
 * @return		la quantita` piu` prossima all'esaurimento,
 * 			da confrontare col parametro 'size' di symtree_init
 * 			
 * @see symtree_init
 * @see symtree_reset_if_no_space
 */
int symtree_used(symtree_t *tree);

#ifdef LZ78_PROFILE

/**
 * stampa sul file indicato alcune statistiche e informazioni sull'albero
 * indicato. Attualmente, questo significa una semplice statistica sulla
 * distribuzione delle profondita` dei rami dell'albero
 *
 * @param tree		puntatore all'albero su cui operare
 * @param f		file su cui stampare i risultati
 *
 * @see symtree_init
 * @see symtree_reset_if_no_space
 */

void symtree_dump(symtree_t *tree,  FILE *f);
		
#endif
		
#endif // _SYMTREE_H_INCLUDED_
