/*
 * lzip78 - bit-level I/O implementation
 * (c) 2005-2013 Francesco Romani <fromani on gmail>
 * License: MIT
 */

#include <stdlib.h>
#include <string.h>

#include "common.h"
#include "bitio.h"

/***************************************************************************
 * private utility functions                                               *
 ***************************************************************************/

/**
 * accumulate avalaible bits from `src' to `dst'.
 * Tries to accumulate as max bit it is possible.
 * Updates both `src' and `dst'.
 * @param dst the destination block.
 * @param src the source block.
 */
static void
bit_merge8(bit_block8_t *dst, bit_block8_t *src) {
    uint8_t mask = 0xff;
    /* how many bits from the source */
    size_t b = MIN(BITBLANKS(dst), src->used);
    mask >>= BITBLANKS(src);

    dst->bits |= ((src->bits & mask) << dst->used);
    src->bits >>= b;

    src->used -= b;
    dst->used += b;
}


/**
 * scarica il buffer su file.
 *
 * @param bio       il descrittore su cui operare
 * @param only_if_full  flag che indica di scaricare il buffer solo se questo
 *          e` effettivamente pieno
 * @return  0   operazione compiuta con successo
 *      1   chiesto di scaricare un buffer completo, ma questo non
 *          lo era.
 *      -1  scrittura fallita
 */
static int
try_to_write(BITIO *bio, int only_if_full) {
    int ret = 0;
    if(!bio) {
        return 0;
    }
    if(only_if_full && BITBLANKS(&bio->buf)) {
        return 1;
    }

    ret = bio->write(&(bio->buf.bits), sizeof(bio->buf.bits), (size_t)1, bio->handle);
    if(1 != ret) {
        return -1;
    }
    bio->buf.bits = 0;
    bio->buf.used = 0;
    return 0;
}


/* stubs (null pattern) */
size_t
null_read(void *ptr, size_t size, size_t nmemb, void *handle) {
    (void)ptr;
    (void)size;
    (void)handle;
    return nmemb;
}

size_t
null_write(const void *ptr, size_t size, size_t nmemb, void *handle) {
    (void)ptr;
    (void)size;
    (void)handle;
    return nmemb;
}

int
null_flush(void *handle) {
    (void)handle;
    return 0;
}


/***************************************************************************
 * API functions                                                           *
 ***************************************************************************/

static BITIO*
bit_open(void *handle, io_read read, io_write write, io_flush flush) {
    BITIO *bio = NULL;

    if(read || write) {
        bio = malloc(sizeof(BITIO));
        if(NULL != bio) {
            bio->buf.bits = 0;
            bio->buf.used = 0;
            bio->readonly = (!write || write == null_write);
            bio->handle = handle;
            bio->read = read ?read :null_read;
            bio->write = write ?write :null_write;
            bio->flush = flush ?flush :null_flush;
        }
    }
    return bio;
}

BITIO *
bit_open_r(void *handle, io_read read) {
    return bit_open(handle, read, NULL, NULL);
}

BITIO *
bit_open_w(void *handle, io_write write, io_flush flush) {
    return bit_open(handle, NULL, write, flush);
}

int
bit_close(BITIO *bio) {
    int ret = 0, err = 0;
    if(bio) {
        if(!bio->readonly && bio->buf.used > 0 ) {
            ret = try_to_write(bio, FALSE);
            if(0 != ret) {
                return -1;
            }

            ret = bio->flush(bio->handle);
            if(0 != ret) {
                return -2;
            }
        }
        free(bio);
    }
    return err;
}

static int
bit_write_block(BITIO *bio, bit_block8_t *buf,
                uint8_t octet, size_t used) {
    int err = 0;

    buf->bits = octet;
    buf->used = used;
    bit_merge8(&bio->buf, buf);

    err = try_to_write(bio, TRUE);
    /* can fail only after writes. */
    if(!err) {
        /*
         * We MUST try again after a (tentative) write
         * to avoid to have spurious bits in the buffer,
         * that will get lost at exit.
         */
        bit_merge8(&bio->buf, buf);
    }
    return err;
}

/*
 * the buffer will contain between 0 and len] (8, right now)
 * MEANINGFUL bits, starting from the LSB (on the right).
 */
int
bit_write(BITIO* bio, const void *data, size_t len) {
    int i = 0, w = 0, ret;
    int blocks = 0, remain = 0;
    const uint8_t *pchunk = NULL;
    bit_block8_t tmpbuf = {0, 0};

    if(!bio || !bio->handle) {
        return -1;
    }
    if(!data) {
        return 0;
    }
    pchunk = data;
    blocks = len >> 3;
    remain = len % 8;

    /* full blocks */
    for(i = 0; i < blocks; i++) {
        ret = bit_write_block(bio, &tmpbuf, pchunk[i], 8);
        if(ret != 0) {
            /* must fail only after writes. */
            return w;
        }
        w += 8;
    }
    if(remain > 0) {
        /* incomplete block. */
        ret = bit_write_block(bio, &tmpbuf, pchunk[i], remain);
        if(ret < 0) {
            /* must fail only after writes. */
            return w;
        }
    }
    return w + remain;
}

static int
bit_read_block(BITIO *bio, bit_block8_t *buf) {
    int ret = bio->read(&buf->bits, sizeof(buf->bits), (size_t)1, bio->handle);
    if(1 == ret) {
        buf->used = 8;
        bit_merge8(&bio->buf, buf);
    }
    return ret;
}


int
bit_read(BITIO* bio, void *data, size_t len) {
    int i = 0, r = 0, ret =0;
    int blocks = 0, remain = 0;
    uint8_t *pchunk = NULL;
    bit_block8_t tmpbuf = {0, 0};

    if(!bio || !bio->handle) {
        return -1;
    }
    if(!data) {
        return 0;
    }
    pchunk = data;
    blocks = len >> 3;
    remain = len % 8;

    /* full blocks */
    for(i = 0; i < blocks; i++) {
        ret = bit_read_block(bio, &tmpbuf);
        if(1 != ret) {
            return r;
        }

        /* we're sure we got a full block */
        pchunk[i] = bio->buf.bits;
        bio->buf.bits = 0;
        bio->buf.used = 0;
        r += 8;

        /*
         * We MUST try again after a (tentative) read
         * to avoid to have spurious bits in the buffer,
         * that will get lost at exit.
         */
        bit_merge8(&bio->buf, &tmpbuf);
    }
    /* incomplete block */
    if(remain > 0) {
        int need_bits = FALSE;
        if(bio->buf.used < remain) {
            /* not enough bits, we need another block. */
            ret = bit_read_block(bio, &tmpbuf);
            if(1 != ret) {
                return r;
            }
            need_bits = TRUE;
        }
        /* we read anyway an incomplete block */
        pchunk[i] = bio->buf.bits & (0xff >> (8 - remain));
        bio->buf.bits >>= remain;
        bio->buf.used -= remain;

        if(need_bits) {
            bit_merge8(&bio->buf, &tmpbuf); /* to be sure. */
        }
    }
    return r + remain;
}

/* EOF */
