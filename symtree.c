/*
 * Francesco Romani 20050324 - 15:00
 * albero di simboli - implementazione
 */

#include <stdlib.h>
#include "symtree.h"

/****************************************************************************
 * funzioni e macro di supporto                                             *
 ****************************************************************************/

#define NODE_GET_FIRST_SON(tree, node) 	(tree->nodes[node].ptr) 
#define NODE_GET_PARENT(tree, node)	(tree->nodes[node].ptr)

#define PTR_GET_NEXT(tree, p)		(tree->ptrs[p].next)
#define PTR_GET_NODE(tree, p)		(tree->ptrs[p].son)

/**
 * NOTA:
 *
 * nei due prototipi sottostanti si usa la signature
 * 
 * 	symtree_add_child_*(symtree_t*, int, int)
 * 
 * anziche` la piu` corretta
 * 
 * 	symtree_add_child_*(symtree_t*, ptr_t, sym_t)
 * 
 * per evitare che gcc fallisca la compilazione, a causa delle CFLAGS
 * utilizzate
 */

/**
 * aggiunge un figlio ad un albero UPSTREAM.
 * Non controlla che l'albero su cui opera sia di tipo UPSTREAM perche`
 * questa funzione non dovrebbe essere mai invocata direttamente, 
 * essendo privata, ma solo tramite symtree_add_child che effettua tutti
 * gli opportuni controlli.
 *
 * @param tu	(Tree Upstream) l'albero in cui inserire il nuovo nodo
 * @param nu	(Node Upstream) puntatore al nodo cui aggiungere il figlio
 * @param su	(Symbol Upstream) simbolo da inserire nel nodo da creare
 * 
 * @return	un puntatore all figlio aggiunto, o SYMTREE_NULL in caso di
 * 		fallimento
 */
static ptr_t
symtree_add_child_up(symtree_t *tu, int nu, int su) {
	/* se node == SYMTREE_NULL aggiungo un figlio alla radice */
	ptr_t p = SYMTREE_NULL;
	ptr_t node = (ptr_t)nu;
	sym_t sym = (sym_t)su;
	
	if(tu->node_last < tu->size) {
		p = tu->node_last++;
		tu->nodes[p].sym = sym;
		tu->nodes[p].ptr = node;
		
		/* supporto per una  _get_path_len efficiente */
		tu->ptrs[p].next = SYMTREE_NULL;
		if(SYMTREE_PTR_ROOT == node) {
			tu->ptrs[p].son = 1;
		} else {
			tu->ptrs[p].son = tu->ptrs[node].son + 1;
		}
	}
	return p;
}

/**
 * aggiunge un figlio ad un albero DOWNSTREAM.
 * Non controlla che l'albero su cui opera sia di tipo DOWNSTREAM perche`
 * questa funzione non dovrebbe essere mai invocata direttamente, 
 * essendo privata, ma solo tramite symtree_add_child che effettua tutti
 * gli opportuni controlli.
 *
 * @param td	(Tree Downstream) l'albero in cui inserire il nuovo nodo
 * @param nd	(Node Downstream) puntatore al nodo cui aggiungere il figlio
 * @param sd	(Symbol Downstream) simbolo da inserire nel nodo da creare
 * 
 * @return	un puntatore all figlio aggiunto, o SYMTREE_NULL in caso di
 * 		fallimento
 */
static ptr_t
symtree_add_child_down(symtree_t *td, int nd, int sd) {
	ptr_t p = SYMTREE_NULL;
	ptr_t son = SYMTREE_NULL, node = (ptr_t)nd;
	ptr_t tmp = SYMTREE_NULL, sym = (sym_t)sd;
		
	if((td->node_last < td->size) && (td->ptr_last < td->size)) {
		/* avanti, c'e` piu` posto! */
		/* nodo */
		p = td->node_last++;
		td->nodes[p].sym = sym;
		td->nodes[p].ptr= SYMTREE_NULL;
	
		/* puntatore */
		tmp = td->ptr_last++;
		td->ptrs[tmp].son = p;
		td->ptrs[tmp].next = SYMTREE_NULL;
		
		son = NODE_GET_FIRST_SON(td, node); 
		if(SYMTREE_NULL == son) {
			td->nodes[node].ptr = tmp;
		} else {
			for(; SYMTREE_NULL != PTR_GET_NEXT(td, son); 
				son = PTR_GET_NEXT(td, son)) {
				;
			}
			td->ptrs[son].next = tmp;
		}
#ifdef LZ78_PROFILE		
		if(SYMTREE_PTR_ROOT == node) {
			td->nodes[p].dist = 1;
		} else {
			td->nodes[p].dist = td->nodes[node].dist + 1;
		}
#endif		
	}

	return p;
}

/****************************************************************************
 * funzioni esportate                                                       *
 ****************************************************************************/

int 
symtree_init(symtree_t *tree, int size, int mode) {
	if(tree && (size > 0 && size < SYMTREE_MAX_NODES)) {
		tree->size = size;
		tree->mode = mode;
		
		tree->nodes = malloc(sizeof(symtree_node_t) * size);
		if(!tree->nodes) {
			return -1;
		}
		tree->ptrs = malloc(sizeof(symtree_child_t) * size);
		if(!tree->ptrs) {
			free(tree->nodes);
			return -1;
		}
		symtree_reset(tree);
		
		return 0;
	}
	return 1;
}
		

void 
symtree_fini(symtree_t *tree) {
	if(tree) {
		symtree_reset(tree);
		
		free(tree->nodes);
		tree->nodes = NULL;

		free(tree->ptrs);
		tree->ptrs = NULL;
	}
}

void
symtree_reset(symtree_t *tree) {
	if(tree) {
		tree->node_last = SYMTREE_PTR_FIRST;
		tree->ptr_last = SYMTREE_PTR_FIRST;
		
		tree->nodes[SYMTREE_PTR_ROOT].ptr = SYMTREE_NULL;
	}
}

int 
symtree_reset_if_no_space(symtree_t *tree) {
	if(!tree) {
		return -1;
	}
	if(symtree_used(tree) >= (tree->size - 1)) {
		symtree_reset(tree);
		return TRUE;
	}
	return FALSE;
}

ptr_t
//symtree_add_child(symtree_t *tree, ptr_t node, sym_t sym)
symtree_add_child(symtree_t *tree, int n, int s) {
	/* se node == SYMTREE_NULL aggiungo un figlio alla radice */
	ptr_t res = SYMTREE_NULL;

	if(!tree) {
		return SYMTREE_NULL;
	}
	/* albero valido */
	if((ptr_t)n > tree->node_last) {
		/* ...ma puntatore non valido */
		return SYMTREE_NULL;
	}
	
	if(SYMTREE_UPSTREAM == tree->mode) {
		res = symtree_add_child_up(tree, n, s);
	} else
	if(SYMTREE_DOWNSTREAM == tree->mode) {
		res = symtree_add_child_down(tree, n, s);
	}
	return res;
}

/* per far contento gcc */
ptr_t 
//symtree_has_child(symtree_t *tree, ptr_t node, sym_t sym) {
symtree_has_child(symtree_t *tree, int n, int s) {
	/* se node == SYMTREE_NULL controllo la radice */
	ptr_t node = (ptr_t)n;
	sym_t sym = (sym_t)s;
	
	ptr_t tmp = SYMTREE_NULL,  p = SYMTREE_NULL;
	int found = FALSE;

	if(!tree) {
		return SYMTREE_NULL;
	}
	if(SYMTREE_DOWNSTREAM != tree->mode) {
		return SYMTREE_NULL;
	}
	/* albero valido e mode == SYMTREE_DOWNSTREAM */
	if(node > tree->node_last) {
		/* ...ma puntatore non valido */
		return SYMTREE_NULL;
	}
	
	p = NODE_GET_FIRST_SON(tree, node);
	for(; SYMTREE_NULL != p; p = PTR_GET_NEXT(tree, p)) {
		tmp = PTR_GET_NODE(tree, p);
		if(NODE_GET_SYM(tree, tmp) == sym) {
			found = TRUE;
			break;
		}
	}
	return (found) ?tmp :SYMTREE_NULL;
}

/* in piu` rispetto alla macro ho 'solo' il bound check */
sym_t 
symtree_get_sym(symtree_t *tree, int n) {
	ptr_t node = (ptr_t)n;
	sym_t sym = 0;
	/* il nodo root non ha MAI un simbolo associato valido */
	if(SYMTREE_NULL != node || node <= tree->node_last) {
		sym = NODE_GET_SYM(tree, node);
	}
	return sym;
}

ptr_t 
symtree_find_ancestor(symtree_t *tree, int n) {
	ptr_t node = (ptr_t)n;
	
	if(!tree || SYMTREE_UPSTREAM != tree->mode) {
		return SYMTREE_NULL;
	}
	if(SYMTREE_PTR_ROOT == node || SYMTREE_NULL == node) {
		return node;
	}
	/**
	 * risalgo l'albero sino al nodo subito sotto la root,
	 * eventualmente puo` essere il puntatore stesso.
	 */
	while(SYMTREE_PTR_ROOT != NODE_GET_PARENT(tree, node)) {
		node = NODE_GET_PARENT(tree, node);
	}
	return node;
}

size_t 
symtree_get_path_len(symtree_t *tree, int node) {
	size_t dist;

	/* 
	 * non ho albero (o ce l'ho sbagliato):
	 * la distanza e` zero perche` non ha senso
	 * calcolarla
	 */
	if(!tree || (SYMTREE_UPSTREAM != node)) {
		dist = 0;
	}
	/*
	 * come sopra, ma solo per un nodo invalido;
	 * inoltre la distanza della radice da se stessa
	 * e` sempre zero
	 */
	if(SYMTREE_PTR_ROOT == node || node > tree->node_last) {
		dist = 0;
	} else {
		/* richiede sincronizzazione con add_child ! */
		dist = tree->ptrs[node].son;
	}
	return dist;
}

/* RICORDA CHE LA RADICE NON HA MAI UN SIMBOLO ASSOCIATO */
int 
symtree_walk(symtree_t *tree, int n, sym_t *buf, size_t bufsize) {
	int ret = -1;
	size_t i = 0;
	ptr_t node = (ptr_t)n;

	if(!tree || !buf) {
		return 0;
	}
	if(SYMTREE_UPSTREAM != tree->mode) {
		return -2;
	}
	
	if(SYMTREE_PTR_ROOT != node && node < tree->node_last) {
		i = symtree_get_path_len(tree, node);
		if(i <= bufsize) {
			/**
			 * inserisco i simboli nel buffer in ordine inverso,
			 * risalendo l'albero verso la radice
			 */
			for(; SYMTREE_NULL != NODE_GET_PARENT(tree, node); 
					node = NODE_GET_PARENT(tree, node)) {
				buf[--i] = NODE_GET_SYM(tree, node);
			}
			ret = 0;
		}
	}
	return ret;
}

int 
symtree_used(symtree_t *tree) {
	if(!tree) {
		return 0;
	}

	/* 
	 * -1 perche` node_last e ptr_last indicano in effetti
	 * il prossimo nodo/blocco da usare. La notazione e` forse
	 * ambigua e sara` cambiata in futuro
	 */
	return MAX(tree->node_last-1, tree->ptr_last-1);
}

#ifdef LZ78_PROFILE

void
symtree_dump(symtree_t *tree, FILE *f) {
	int dists[16] = {0, };
	int i = 0, d = 0;
	
	if(!tree || !f) {
		return;
	}
	fputs("\n", f);
	fprintf(f, "(%s) symtree @[%p]: size=%i, nodi=%i, puntatori=%i\n",
			__FILE__, tree, tree->size, 
			tree->node_last, tree->ptr_last);
	
	for(i = 0; i < tree->size; i++) {
		if(SYMTREE_DOWNSTREAM == tree->mode) {
			d = tree->nodes[i].dist;
		} else {
			d = tree->ptrs[i].son;
		}
		d = CLAMP(d, 0, 15);
		dists[d]++;
	}

	fprintf(f, "(%s) distribuzione distanze:\n", __FILE__);
	for(i = 0; i < 16; i++) {
		fprintf(f, "(%s) distanza %s %02i: %02i\n",
			__FILE__, (15 == i) ?">=" :" =", i, dists[i]);
	}
	fputs("\n", f);
	fflush(f);
}

#endif
/* EOF */
