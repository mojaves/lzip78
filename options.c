/*
 * lzip78 - option handling implementation.
 * (c) 2005-2013 Francesco Romani <fromani on gmail>
 * License: MIT
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

extern char *optarg;
extern int optind;
extern int optopt;
extern int opterr;
extern int optreset;

#include "common.h"
#include "options.h"
#include "lz78.h"

void
options_default(options_t *opts) {
    if(opts) {
        opts->mode = LZ78_COMPRESS;
        opts->level = LZ78_LEVEL_DEF;
        opts->verbose = 0;
        opts->infile = "stdin";
        opts->suffix = DEFAULT_EXT;
        opts->use_stdio = FALSE;
        opts->bitstream_ver = 2;
        opts->block_size = BLOCK_SIZE_DEFAULT;
    }
}

int
options_parse_cmdline(options_t *opts, int argc, char *argv[]) {
    int ch = 0, parsed = 0, show_help = TRUE;

    while(-1 != (ch = getopt(argc, argv, "+123456789LVB:b:cds:vh"))) {
        switch(ch) {
            case '1': /* fallback */
            case '2': /* fallback */
            case '3': /* fallback */
            case '4': /* fallback */
            case '5': /* fallback */
            case '6': /* fallback */
            case '7': /* fallback */
            case '8': /* fallback */
            case '9':
                opts->level = ch - '1';
                /* i livelli interni vanno da 0 a 8 */
                break;
            case 'B':
                if(!optarg || !strlen(optarg)) {
                    parsed = -1;
                } else {
                    opts->block_size = atoi(optarg);
                    if(opts->block_size < BLOCK_SIZE_MIN ||
                       opts->block_size > BLOCK_SIZE_MAX) {
                        parsed = -1;
                    }
                }
                break;
            case 'b':
                if(!optarg || !strlen(optarg)) {
                    parsed = -1;
                } else {
                    opts->bitstream_ver = atoi(optarg);
                    if(opts->bitstream_ver < 1 ||
                       opts->bitstream_ver > 2) {
                        parsed = -1;
                    }
                }
                break;
            case 'c':
                opts->use_stdio = TRUE;
                break;
            case 'd':
                opts->mode = LZ78_DECOMPRESS;
                break;
            case 's':
                if(!optarg || !strlen(optarg)) {
                    parsed = -1;
                } else {
                    opts->suffix = optarg;
                }
                break;
            case 'v':
                opts->verbose++;
                parsed++;
                break;
            case 'V':
                options_show_version(argv[0]);
                parsed = -1; /* forza l'uscita */
                break;
            case 'L':
                options_show_license(argv[0]);
                parsed = -1;
                break;
            case 'h':
                options_show_version(argv[0]);
                options_show_license(argv[0]);
                options_show_help(argv[0]);
                parsed = -1; /* forza l'uscita */
                show_help = FALSE;
                break;
            default:
                options_show_help(argv[0]);
                show_help = FALSE;
                parsed = -1;
                break;

        }
        if(-1 == parsed) { /* c'e` un'errore... */
            break;
        }
    }

    if(NULL != strstr(argv[0], DECOMPRESSOR_NAME)) {
        opts->mode = LZ78_DECOMPRESS;
    }

    if(optind < argc) {
        opts->infile = argv[optind];
        if(!strcmp(opts->infile, "-")) {
            opts->use_stdio = TRUE;
        }
        parsed++;
    }

    if(show_help && 0 == parsed) {
        options_show_help(argv[0]);
    }
    /* forza il modo a seconda del nome dell'eseguibile */

    return parsed;
}

void
options_show_help(const char *program) {
    fprintf(stderr, "utilizzo: %s [opzioni] FILE\n", program);
    fputs("Comprime FILE usando le opzioni indicate; se FILE e` '-'"
        "in questo caso l'output sara` su stdout\n", stderr);
    fputs("opzioni disponibili:\n", stderr);
    fprintf(stderr, "\t-# (1-9)   livello di compressione da utilizzare: "
                            "1: minimo, 9: massimo [%i]\n", LZ78_LEVEL_DEF);
    fputs("\t-L         mostra la licenza ed esce\n", stderr);
    fputs("\t-V         mostra la versione ed esce\n", stderr);
//  fputs("\t-s [suf]   usa [suf] come suffisso per il file "
//      "compresso, non '"DEFAULT_EXT"'\n", stderr);
    fprintf(stderr,
            "\t-B <size>  usa <size> come block size (bytes) [%i]\n",
            BLOCK_SIZE_DEFAULT);
    fputs("\t-b <ver>   usa la versione di bitstream <ver> [1]\n",
          stderr);
    fputs("\t-c         forza l'I/O su stdout anche se non dovrebbe\n",
          stderr);
    fputs("\t-d         forza la decompressione\n", stderr);
    fputs("\t-v         piu` messaggi durante l'esecuzione\n", stderr);
    fputs("\t-h         mostra questo messaggio\n", stderr);
}

void
options_show_version(const char *program) {
    fprintf(stderr, "%s version %s (%s) - "
        "(C) Francesco Romani - fromani@gmail.com\n",
        program, VERSION, __DATE__);
}

void
options_show_license(const char *program) {
    fprintf(stderr, "%s e` rilasciato sotto i termini della licenza "
            "MIT allegata\n", program);
}

/* EOF */
