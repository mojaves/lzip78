/*
 * test di bitio
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/uio.h>

#include "bitio.h"

/*
 * interfaccia di test:
 * ogni funzione implementa uno specirfico test
 * schema di nomi:
 * test_cosa_viene_testato()
 *
 * ogni funzione non dovrebbe accettare parametri
 * valore di ritorno:
 * 0	test passato
 * !0	test fallito
 */
typedef int (*test_func)(void);

void
runtest(const char *desc, test_func func)
{
	int ret;
	fprintf(stderr, "testing: '%s'\n", desc);
	ret = func();
	fprintf(stderr, "%s: %s\n", desc, 
		(0 == ret) ?"passato" :"FALLITO <<<----------------------------");
}		

int
test_open_write_close(void) {
	BITIO *bio;
	bio = bit_open("test0.dat", BITIO_WRITE);
	if(!bio) {
		fprintf(stderr, "apertura (in scrittura) fallita\n");
		return 1;
	}	
	if(0 != bit_close(bio)) {
		fprintf(stderr, "chiusura fallita\n");
		return 1;
	}	
	return 0;
}

int
test_open_read_close(void) {
	BITIO *bio;
	bio = bit_open("test0.dat", BITIO_READ);
	if(!bio) {
		fprintf(stderr, "apertura (in lettura) fallita\n");
		return 1;
	}	
	if(0 != bit_close(bio)) {
		fprintf(stderr, "lettura fallita\n");
		return 1;
	}	
	return 0;
}

int
test_write_string(void) {
	BITIO *bio;
	int ret;

	unlink("test1.dat");
	
	bio = bit_open("test1.dat", BITIO_WRITE);
	if(!bio) {
		fprintf(stderr, "apertura (in scrittura) fallita\n");
		return 1;
	}
	ret = bit_write(bio, "pippo", (size_t)40);
	if(40 != ret) {
		fprintf(stderr, "scrittura fallita (%i invece di 40)\n", ret);
		return 1;
	}
	
	if(0 != bit_close(bio)) {
		fprintf(stderr, "chiusura fallita\n");
		return 6;
	}
	return 0;
}

int
test_read_string(void) {
	BITIO *bio;
	int ret;
	char buf[8] = {'\0'};

	bio = bit_open("test1.dat", BITIO_READ);
	if(!bio) {
		fprintf(stderr, "apertura (in lettura) fallita\n");
		return 1;
	}
	ret = bit_read(bio, &buf, (size_t)40);
	if(40 != ret) {
		fprintf(stderr, "lettura fallita (%i invece di 40)\n", ret);
		return 1;
	}

	if(0 != strcmp("pippo", buf)) {
		fprintf(stderr, "la stringa letta non corrisponde: ");
		fprintf(stderr, "buf=%x%x%x%x%x (%s)\n", 
				buf[0], buf[1], buf[2], buf[3], buf[4], 
				buf);
		return 1;
	}
	
	if(0 != bit_close(bio)) {
		fprintf(stderr, "lettura fallita\n");
		return 1;
	}
	return 0;
}

int
test_write_chunk_partial(void) {
	BITIO *bio;
	int ret;
	uint8_t c1 = 6;
	uint8_t c2 = 5;

	unlink("test2.dat");
	
	bio = bit_open("test2.dat", BITIO_WRITE);
	if(!bio) {
		fprintf(stderr, "apertura (in scrittura) fallita\n");
		return 1;
	}
	ret = bit_write(bio, &c2, (size_t)1);
	if(1 != ret) {
		fprintf(stderr, "scrittura fallita (%i invece di 1)\n", ret);
		return 1;
	}
	
	ret = bit_write(bio, &c1, (size_t)2);
	if(2 != ret) {
		fprintf(stderr, "scrittura fallita (%i invece di 2)\n", ret);
		return 1;
	}
	
	if(0 != bit_close(bio)) {
		fprintf(stderr, "chiusura fallita\n");
		return 2;
	}
	return 0;
}	

int
test_read_chunk_partial(void) {
	BITIO *bio;
	int ret;
	uint8_t c = '\0';
	uint8_t cc = '\0';

	bio = bit_open("test2.dat", BITIO_READ);
	if(!bio) {
		fprintf(stderr, "apertura (in lettura) fallita\n");
		return 1;
	}
	ret = bit_read(bio, &cc, (size_t)1);
	if(1 != ret) {
		fprintf(stderr, "lettura fallita (%i invece di 1)\n", ret);
		return 1;
	}
	c |= cc;
	
	ret = bit_read(bio, &cc, (size_t)2);
	if(2 != ret) {
		fprintf(stderr, "lettura fallita (%i invece di 2)\n", ret);
		return 1;
	}
	c |= (cc << 1);
	
	if(5 != c) {
		fprintf(stderr, "lettura totale (%c [%x] invece di '5')\n", c, c);
		return 1;
	}
	
	if(0 != bit_close(bio)) {
		fprintf(stderr, "chiusura fallita\n");
		return 2;
	}
	return 0;
}

int
test_write_chunk(void) {
	BITIO *bio;
	int ret;
	uint8_t c1 = 0x5;
	uint8_t c2 = 0xa;

	unlink("test3.dat");
	
	bio = bit_open("test3.dat", BITIO_WRITE);
	if(!bio) {
		fprintf(stderr, "apertura (in scrittura) fallita\n");
		return 1;
	}
	ret = bit_write(bio, &c2, (size_t)1); // '0'
	if(1 != ret) {
		fprintf(stderr, "scrittura fallita (%i invece di 1)\n", ret);
		return 1;
	}
	
	ret = bit_write(bio, &c1, (size_t)2); // '01'
	if(2 != ret) {
		fprintf(stderr, "scrittura fallita (%i invece di 2)\n", ret);
		return 1;
	}
	
	ret = bit_write(bio, &c1, (size_t)1); // '1'
	if(1 != ret) {
		fprintf(stderr, "scrittura fallita (%i invece di 1)\n", ret);
		return 1;
	}
	
	ret = bit_write(bio, &c1, (size_t)3); // '101'
	if(3 != ret) {
		fprintf(stderr, "scrittura fallita (%i invece di 3)\n", ret);
		return 1;
	}
	
	ret = bit_write(bio, &c2, (size_t)1); // '0'
	if(1 != ret) {
		fprintf(stderr, "scrittura fallita (%i invece di 1)\n", ret);
		return 1;
	}
	
	if(0 != bit_close(bio)) {
		fprintf(stderr, "chiusura fallita\n");
		return 2;
	}
	return 0;
}

int
test_read_chunk(void) {
	BITIO *bio;
	int ret;
	uint8_t c;
	uint8_t cc;

	bio = bit_open("test3.dat", BITIO_READ);
	if(!bio) {
		fprintf(stderr, "apertura (in lettura) fallita\n");
		return 1;
	}
	ret = bit_read(bio, &cc, (size_t)1); // '0'
	if(1 != ret) {
		fprintf(stderr, "lettura fallita (%i invece di 1)\n", ret);
		return 1;
	}
	c = cc;
	
	ret = bit_read(bio, &cc, (size_t)2); // '01'
	if(2 != ret) {
		fprintf(stderr, "lettura fallita (%i invece di 2)\n", ret);
		return 1;
	}
	c |= cc << 1;
	
	ret = bit_read(bio, &cc, (size_t)1); // '1'
	if(1 != ret) {
		fprintf(stderr, "lettura fallita (%i invece di 1)\n", ret);
		return 1;
	}
	c |= cc << 3;
	
	ret = bit_read(bio, &cc, (size_t)3); // '101'
	if(3 != ret) {
		fprintf(stderr, "lettura fallita (%i invece di 3)\n", ret);
		return 1;
	}
	c |= cc << 4;
	
	ret = bit_read(bio, &cc, (size_t)1); // '0'
	if(1 != ret) {
		fprintf(stderr, "lettura fallita (%i invece di 1)\n", ret);
		return 1;
	}
	c |= cc << 7;
	
	if(0x5a != c) {
		fprintf(stderr, "lettura totale (%x invece di 0x5a)\n", c);
		return 1;
	}
	
	if(0 != bit_close(bio)) {
		fprintf(stderr, "chiusura fallita\n");
		return 2;
	}
	return 0;
}

int
test_read_chunk_mixed(void) {
	BITIO *bio;
	int ret;
	uint16_t c = 0;

	bio = bit_open("test4.dat", BITIO_READ);
	if(!bio) {
		fprintf(stderr, "apertura (in lettura) fallita\n");
		return 1;
	}
	ret = bit_read(bio, &c, (size_t)11);
	if(11 != ret) {
		fprintf(stderr, "lettura fallita (%i invece di 11)\n", ret);
		return 1;
	}
	if(0x03dc != c) {
		fprintf(stderr, "lettura errata: %x invece di %x\n", c, 0x03dc);
		return 1;
	}
	c = 0;
	
	ret = bit_read(bio, &c, (size_t)9);
	if(9 != ret) {
		fprintf(stderr, "lettura fallita (%i invece di 9)\n", ret);
		return 1;
	}
	if(0x01dc != c) {
		fprintf(stderr, "lettura errata: %x invece di %x\n", c, 0x03dc);
		return 1;
	}
	c = 0;

	ret = bit_read(bio, &c, (size_t)12);
	if(12 != ret) {
		fprintf(stderr, "lettura fallita (%i invece di 12)\n", ret);
		return 1;
	}
	if(0x0bdc != c) {
		fprintf(stderr, "lettura errata: %x invece di %x\n", c, 0x03dc);
		return 1;
	}
	c = 0;

	if(0 != bit_close(bio)) {
		fprintf(stderr, "chiusura fallita\n");
		return 2;
	}
	return 0;
}

int
test_write_chunk_mixed(void) {
	BITIO *bio;
	int ret;
	uint16_t c = 0x3bdc;

	unlink("test4.dat");
	
	bio = bit_open("test4.dat", BITIO_WRITE);
	if(!bio) {
		fprintf(stderr, "apertura (in scrittura) fallita\n");
		return 1;
	}
	ret = bit_write(bio, &c, (size_t)11);
	if(11 != ret) {
		fprintf(stderr, "scrittura fallita (%i invece di 11)\n", ret);
		return 1;
	}
	ret = bit_write(bio, &c, (size_t)9);
	if(9 != ret) {
		fprintf(stderr, "scrittura fallita (%i invece di 9)\n", ret);
		return 1;
	}
	ret = bit_write(bio, &c, (size_t)12);
	if(12 != ret) {
		fprintf(stderr, "scrittura fallita (%i invece di 12)\n", ret);
		return 1;
	}

	if(0 != bit_close(bio)) {
		fprintf(stderr, "chiusura fallita\n");
		return 2;
	}
	return 0;
}
int
test_write_chunk_random(void) {
	BITIO *bio;
	int ret;
	uint16_t c = 0x3bdc;

	unlink("test5.dat");
	
	bio = bit_open("test5.dat", BITIO_WRITE);
	if(!bio) {
		fprintf(stderr, "apertura (in scrittura) fallita\n");
		return 1;
	}
	ret = bit_write(bio, &c, (size_t)5);
	if(5 != ret) {
		fprintf(stderr, "scrittura fallita (%i invece di 5)\n", ret);
		return 1;
	}
	
	ret = bit_write(bio, &c, (size_t)7);
	if(7 != ret) {
		fprintf(stderr, "scrittura fallita (%i invece di 7)\n", ret);
		return 1;
	}
	
	ret = bit_write(bio, &c, (size_t)11);
	if(11 != ret) {
		fprintf(stderr, "scrittura fallita (%i invece di 11)\n", ret);
		return 1;
	}
	
	ret = bit_write(bio, &c, (size_t)3);
	if(3 != ret) {
		fprintf(stderr, "scrittura fallita (%i invece di 3)\n", ret);
		return 1;
	}
	
	ret = bit_write(bio, &c, (size_t)4);
	if(4 != ret) {
		fprintf(stderr, "scrittura fallita (%i invece di 4)\n", ret);
		return 1;
	}
	
	if(0 != bit_close(bio)) {
		fprintf(stderr, "chiusura fallita\n");
		return 2;
	}
	return 0;
}	

int
test_read_chunk_random(void) {
	BITIO *bio;
	int ret;
	uint16_t c = 0;
	
	bio = bit_open("test5.dat", BITIO_READ);
	if(!bio) {
		fprintf(stderr, "apertura (in lettura) fallita\n");
		return 1;
	}
	ret = bit_read(bio, &c, (size_t)5);
	if(5 != ret) {
		fprintf(stderr, "lettura fallita (%i invece di 5)\n", ret);
		return 1;
	}
	if(0x1c != c) {
		fprintf(stderr, "lettura errata: %x invece di %x\n", c, 0x1c);
		return 1;
	}
	c = 0;
	
	ret = bit_read(bio, &c, (size_t)7);
	if(7 != ret) {
		fprintf(stderr, "scrittura fallita (%i invece di 7)\n", ret);
		return 1;
	}
	if(0x5c != c) {
		fprintf(stderr, "lettura errata: %x invece di %x\n", c, 0x5c);
		return 1;
	}
	c = 0;
	
	ret = bit_read(bio, &c, (size_t)11);
	if(11 != ret) {
		fprintf(stderr, "scrittura fallita (%i invece di 11)\n", ret);
		return 1;
	}
	if(0x03dc != c) {
		fprintf(stderr, "lettura errata: %x invece di %x\n", c, 0x03dc);
		return 1;
	}
	c = 0;
	
	ret = bit_read(bio, &c, (size_t)3);
	if(3 != ret) {
		fprintf(stderr, "scrittura fallita (%i invece di 3)\n", ret);
		return 1;
	}
	if(0x4 != c) {
		fprintf(stderr, "lettura errata: %x invece di %x\n", c, 0x4);
		return 1;
	}
	c = 0;
	
	ret = bit_read(bio, &c, (size_t)4);
	if(4 != ret) {
		fprintf(stderr, "scrittura fallita (%i invece di 4)\n", ret);
		return 1;
	}
	if(0xc != c) {
		fprintf(stderr, "lettura errata: %x invece di %x\n", c, 0xc);
		return 1;
	}
	c = 0;
	
	if(0 != bit_close(bio)) {
		fprintf(stderr, "chiusura fallita\n");
		return 2;
	}
	return 0;
}

int 
main(void) {
	runtest("test_open_write_close", test_open_write_close);
	runtest("test_write_string", test_write_string);
	runtest("test_write_chunk_partial", test_write_chunk_partial);
	runtest("test_write_chunk", test_write_chunk);
	runtest("test_write_chunk_mixed", test_write_chunk_mixed);
	runtest("test_write_chunk_random", test_write_chunk_random);

	runtest("test_open_read_close", test_open_read_close);
	runtest("test_read_string", test_read_string);
	runtest("test_read_chunk_partial", test_read_chunk_partial);
	runtest("test_read_chunk", test_read_chunk);
	runtest("test_read_chunk_mixed", test_read_chunk_mixed);
	runtest("test_read_chunk_random", test_read_chunk_random);

	return 0;
}	
