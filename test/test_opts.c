/*
 * test per la gestione delle opzioni
 */

#include <stdio.h>
#include "options.h"
#include "lz78.h"

int main(int argc, char* argv[]) {
	options_t opts;

	options_default(&opts);
	if(options_parse_cmdline(&opts, argc, argv) > 0) {
		printf("modo operativo = %s\n", 
			(LZ78_DECOMPRESS == opts.mode) 
			?"decompressore" :"compressore");
		printf("verbose        = %i\n", opts.verbose);
		printf("livello        = %i\n", opts.level);
		printf("su stdio       = %s\n", 
			(opts.use_stdio) ?"si" :"no");
		printf("infile         = %s\n", opts.infile);
		printf("suffix         = %s\n", opts.suffix);
		if(opts.use_stdio) {
			printf("outfile        = -\n");
		} else {
			printf("outfile        = %s.%s\n", opts.infile, opts.suffix);
		}
	}

        if(LZ78_COMPRESS == opts.mode) {
		puts("["__FILE__"] modalita` compressore\n");
	} else if(LZ78_DECOMPRESS == opts.mode) {
		puts("["__FILE__"] modalita` decompressore\n");
	} else {
		puts("["__FILE__"] modalita` di funzionamento ignota "
			"(questo non dovrebbe succedere)\n");
	}
	return 0;
}	
