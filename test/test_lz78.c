/*
 * test di bitio.h
 */

#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/uio.h>

#include "lz78.h"

/*
 * interfaccia di test:
 * ogni funzione implementa uno specirfico test
 * schema di nomi:
 * test_cosa_viene_testato()
 *
 * ogni funzione non dovrebbe accettare parametri
 * valore di ritorno:
 * 0	test passato
 * !0	test fallito
 */
typedef int (*test_func)(void);

void
runtest(const char *desc, test_func func)
{
	int ret;
	printf("testing '%s' :\n", desc);
	fflush(stdout);
	ret = func();
	printf("\t%s\n", (0 == ret) ?"passato" :"FALLITO <<<---------------------");
	fflush(stdout);
}		

int
test_open_close_compr(void) {
	lz78_context_t lz78;
	int err;

	err = lz78_init(&lz78, LZ78_DEFAULT_FLAGS|LZ78_COMPRESS, LZ78_LEVEL_DEF);
	if(err) {
		fprintf(stderr, "apertura compressore fallita\n");
		return 1;
	}
	err = lz78_fini(&lz78);
	if(err) {
		fprintf(stderr, "chiusura compressore fallita\n");
		return 1;
	}
	return 0;
}

int
test_open_close_decompr(void) {
	lz78_context_t lz78;
	int err;

	err = lz78_init(&lz78, LZ78_DEFAULT_FLAGS|LZ78_DECOMPRESS, LZ78_LEVEL_DEF);
	if(err) {
		fprintf(stderr, "apertura decompressore fallita\n");
		return 1;
	}
	err = lz78_fini(&lz78);
	if(err) {
		fprintf(stderr, "chiusura decompressore fallita\n");
		return 1;
	}
	return 0;
}

int
test_decompr_simpler(void) {
	lz78_context_t lz78;
	int err;

	err = lz78_init(&lz78, LZ78_DEFAULT_FLAGS|LZ78_DECOMPRESS, LZ78_LEVEL_DEF);
	if(err) {
		fprintf(stderr, "apertura decompressore fallita\n");
		return 1;
	}

	err = lz78_fini(&lz78);
	if(err) {
		fprintf(stderr, "chiusura decompressore fallita\n");
		return 1;
	}
	return 0;
}
int 
main(void) {
	runtest("test_open_close_compr", test_open_close_compr);
	runtest("test_open_close_decompr", test_open_close_decompr);
	
	
	runtest("test_decompr_simpler", test_decompr_simpler);
	
	return 0;
}	
