/*
 * test di bitio.h
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/uio.h>

#include "symtree.h"

/*
 * interfaccia di test:
 * ogni funzione implementa uno specirfico test
 * schema di nomi:
 * test_cosa_viene_testato()
 *
 * ogni funzione non dovrebbe accettare parametri
 * valore di ritorno:
 * 0	test passato
 * !0	test fallito
 */
typedef int (*test_func)(void);

void
runtest(const char *desc, test_func func)
{
	int ret;
	printf("testing '%s' :\n", desc);
	fflush(stdout);
	ret = func();
	printf("\t%s\n", (0 == ret) ?"passato" :"FALLITO <<<---------------------");
	fflush(stdout);
}		

int
test_open_close_up(void) {
	symtree_t tree;
	int err;

	err = symtree_init(&tree, 4096, SYMTREE_UPSTREAM);
	if(err) {
		fprintf(stderr, "apertura fallita\n");
		return 1;
	}	
	symtree_fini(&tree);
	return 0;
}

int
test_open_close_down(void) {
	symtree_t tree;
	int err;

	err = symtree_init(&tree, 4096, SYMTREE_DOWNSTREAM);
	if(err) {
		fprintf(stderr, "apertura fallita\n");
		return 1;
	}	
	symtree_fini(&tree);
	return 0;
}

int
test_insert_root_down(void) {
	symtree_t tree;
	ptr_t p = SYMTREE_PTR_ROOT, s = SYMTREE_NULL;
	sym_t c;
	int i = 0;
	int err;

	err = symtree_init(&tree, 256, SYMTREE_DOWNSTREAM);
	if(err) {
		fprintf(stderr, "apertura fallita\n");
		return 1;
	}	
	c = 'a';
	for(i = 0; i < 8; i++) {
		s = symtree_add_child(&tree, p, c++);
		if(SYMTREE_NULL == s) {
			fprintf(stderr, "inserzione alla radice fallita\n");
			return 1;
		}
		if(s != SYMTREE_PTR_FIRST+i) {
			fprintf(stderr, "ERRORE: l'ultimo nodo NON e` %i ma %i\n",
					SYMTREE_PTR_FIRST+i, s);
			return 1;
		}
	}
	
	symtree_fini(&tree);
	return 0;
}

int
test_insert_root_up(void) {
	symtree_t tree;
	ptr_t p = SYMTREE_PTR_ROOT, s = SYMTREE_NULL;
	sym_t c;
	int i = 0;
	int err;

	err = symtree_init(&tree, 256, SYMTREE_UPSTREAM);
	if(err) {
		fprintf(stderr, "apertura fallita\n");
		return 1;
	}	
	c = 'a';
	for(i = 0; i < 8; i++) {
		s = symtree_add_child(&tree, p, c++);
		if(SYMTREE_NULL == s) {
			fprintf(stderr, "inserzione alla radice fallita\n");
			return 1;
		}
		if(s != SYMTREE_PTR_FIRST+i) {
			fprintf(stderr, "ERRORE: l'ultimo nodo NON e` %i ma %i\n",
					SYMTREE_PTR_FIRST+i, s);
			return 1;
		}
	}
	
	symtree_fini(&tree);
	return 0;
}

int
test_insert_root_chained_down(void) {
	symtree_t tree;
	ptr_t p = SYMTREE_PTR_ROOT, s = SYMTREE_NULL;
	sym_t c;
	int i = 0;
	int err;

	err = symtree_init(&tree, 256, SYMTREE_DOWNSTREAM);
	if(err) {
		fprintf(stderr, "apertura fallita\n");
		return 1;
	}	
	c = 'a';
	for(i = 0; i < 8; i++) {
		s = symtree_add_child(&tree, p, c++);
		if(SYMTREE_NULL == s) {
			fprintf(stderr, "inserzione alla radice fallita\n");
			return 1;
		}
		if(s != SYMTREE_PTR_FIRST+i) {
			fprintf(stderr, "ERRORE: l'ultimo nodo NON e` %i ma %i\n",
					SYMTREE_PTR_FIRST+i, s);
			return 1;
		}
		p = s;
	}

	for(i = SYMTREE_PTR_FIRST; i < (SYMTREE_PTR_FIRST + 8); i++) {
		fprintf(stderr, "node %i: sym = %x (%c) ptr = %i pptr=%i;\n",
			i, tree.nodes[i].sym, tree.nodes[i].sym,
			tree.nodes[i].ptr, tree.ptrs[tree.nodes[i].ptr].son);
	}
	
	symtree_fini(&tree);
	return 0;
}

int
test_insert_root_chained_up(void) {
	symtree_t tree;
	ptr_t p = SYMTREE_PTR_ROOT, s = SYMTREE_NULL;
	sym_t c;
	int i = 0;
	int err;

	err = symtree_init(&tree, 256, SYMTREE_UPSTREAM);
	if(err) {
		fprintf(stderr, "apertura fallita\n");
		return 1;
	}	
	c = 'a';
	for(i = 0; i < 8; i++) {
		s = symtree_add_child(&tree, p, c++);
		if(SYMTREE_NULL == s) {
			fprintf(stderr, "inserzione alla radice fallita\n");
			return 1;
		}
		if(s != SYMTREE_PTR_FIRST+i) {
			fprintf(stderr, "ERRORE: l'ultimo nodo NON e` %i ma %i\n",
					SYMTREE_PTR_FIRST+i, s);
			return 1;
		}
		p = s;
	}
	for(i = SYMTREE_PTR_FIRST; i < (SYMTREE_PTR_FIRST + 8); i++) {
		fprintf(stderr, "node %i: sym = %x (%c) ptr = %i dist = %i;\n",
			i, tree.nodes[i].sym, tree.nodes[i].sym,
			tree.nodes[i].ptr, tree.ptrs[i].son);
	}
	
	
	symtree_fini(&tree);
	return 0;
}

int
test_insert_and_find_chained_down(void) {
	symtree_t tree;
	ptr_t p = SYMTREE_PTR_ROOT, s = SYMTREE_NULL;
	sym_t c;
	int i = 0;
	int err;

	err = symtree_init(&tree, 256, SYMTREE_DOWNSTREAM);
	if(err) {
		fprintf(stderr, "apertura fallita\n");
		return 1;
	}	
	c = 'a';
	for(i = 0; i < 8; i++) {
		s = symtree_add_child(&tree, p, c++);
		if(SYMTREE_NULL == s) {
			fprintf(stderr, "inserzione alla radice fallita\n");
			return 1;
		}
		if(s != SYMTREE_PTR_FIRST+i) {
			fprintf(stderr, "ERRORE: l'ultimo nodo NON e` %i ma %i\n",
					SYMTREE_PTR_FIRST+i, s);
			return 1;
		}
		p = s;
	}
	c = 'a';
	p = SYMTREE_PTR_ROOT;
	for(i = 0; i < 8; i++) {
		s = symtree_has_child(&tree, p, c);
		if(SYMTREE_NULL == s) {
			fprintf(stderr, "ricerca (i=%i) fallita per figlio(%i, %c)\n", i, p, c);
			return 1;
		}
		c++;
		p = s;
	}

	for(i = SYMTREE_PTR_FIRST; i < (SYMTREE_PTR_FIRST + 8); i++) {
		fprintf(stderr, "node %i: sym = %x (%c) ptr = %i pptr=%i;\n",
			i, tree.nodes[i].sym, tree.nodes[i].sym,
			tree.nodes[i].ptr, tree.ptrs[tree.nodes[i].ptr].son);
	}
	
	symtree_fini(&tree);
	return 0;
}
int
test_insert_verify_root_chained_down(void) {
	symtree_t tree;
	ptr_t p = SYMTREE_PTR_ROOT, s = SYMTREE_NULL;
	sym_t c;
	int i = 0;
	int err;

	err = symtree_init(&tree, 256, SYMTREE_DOWNSTREAM);
	if(err) {
		fprintf(stderr, "apertura fallita\n");
		return 1;
	}	
	c = 'a';
	for(i = 0; i < 8; i++) {
		s = symtree_add_child(&tree, p, c++);
		if(SYMTREE_NULL == s) {
			fprintf(stderr, "inserzione alla radice fallita\n");
			return 1;
		}
		if(s != SYMTREE_PTR_FIRST+i) {
			fprintf(stderr, "ERRORE: l'ultimo nodo NON e` %i ma %i\n",
					SYMTREE_PTR_FIRST+i, s);
			return 1;
		}
		if(SYMTREE_NULL == symtree_has_child(&tree, p, (c-1))) {
			fprintf(stderr, "ERRORE: non ritrovo il figlio appena inserito "
					"per il nodo %i", p);
			return 1;
		}
		p = s;
	}

	for(i = SYMTREE_PTR_FIRST; i < (SYMTREE_PTR_FIRST + 8); i++) {
		fprintf(stderr, "node %i: sym = %x (%c) ptr = %i pptr=%i;\n",
			i, tree.nodes[i].sym, tree.nodes[i].sym,
			tree.nodes[i].ptr, tree.ptrs[tree.nodes[i].ptr].son);
	}
	
	symtree_fini(&tree);
	return 0;
}

int
test_reset_down_1(void) {
	symtree_t tree;
	ptr_t p = SYMTREE_PTR_ROOT, s = SYMTREE_NULL;
	sym_t c;
	int i = 0;
	int err;

	err = symtree_init(&tree, 256, SYMTREE_DOWNSTREAM);
	fprintf(stderr, "dopo init()      : used = %i\n", symtree_used(&tree));
	if(err) {
		fprintf(stderr, "apertura fallita\n");
		return 1;
	}	
	c = 'a';
	for(i = 0; i < 8; i++) {
		s = symtree_add_child(&tree, p, c++);
		if(SYMTREE_NULL == s) {
			fprintf(stderr, "inserzione alla radice fallita\n");
			return 1;
		}
		if(s != SYMTREE_PTR_FIRST+i) {
			fprintf(stderr, "ERRORE: l'ultimo nodo NON e` %i ma %i\n",
					SYMTREE_PTR_FIRST+i, s);
			return 1;
		}
	}

	fprintf(stderr, "prima di reset() : used = %i\n", symtree_used(&tree));
	symtree_reset(&tree);
	fprintf(stderr, "dopo di reset()  : used = %i\n", symtree_used(&tree));
	
	c = 'a';
	for(i = 0; i < 8; i++) {
		s = symtree_has_child(&tree, p, c++);
		if(SYMTREE_NULL != s) {
			fprintf(stderr, "reset() fallito: figlio '%c' trovato!\n", c);
			return 1;
		}
	}
	
	symtree_fini(&tree);
	return 0;
}

int main() {
	runtest("test_open_close_up", test_open_close_up);
	runtest("test_open_close_down", test_open_close_up);
	
	runtest("test_insert_root_down", test_insert_root_down);
	runtest("test_insert_root_up", test_insert_root_up);
	
	runtest("test_insert_root_chained_down", test_insert_root_chained_down);
	runtest("test_insert_root_chained_up", test_insert_root_chained_up);
	
	runtest("test_insert_verify_root_chained_down", test_insert_root_chained_down);

	runtest("test_insert_and_find_chained_down", test_insert_and_find_chained_down);
	runtest("test_reset_down1", test_reset_down_1);
	
	return 0;
}	
