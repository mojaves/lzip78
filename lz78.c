/*
 * lzip78 - de/compressor high level interface
 * (c) 2005-2013 Francesco Romani <fromani on gmail>
 * License: MIT
 */


#include <string.h>
#include <stdlib.h>

#include "lz78.h"


/***************************************************************************
 * macro di uso comune                                                     *
 ***************************************************************************/

#define LZ78_COMMON_CHECK \
    if(!ctx) { \
        return -1; \
    } \
    if(!src || !dst) { \
        ctx->errnum = LZ78_ERR_BAD_FILE; \
        return -2; \
    }\

#define LZ78_COMMON_EPILOGUE \
    if(ctx->verbose >= 4) { \
        lz78_print_stats(ctx, stderr); \
    } \
    if(ctx->verbose >= 2) { \
        fputs("terminato\n", stderr); \
    }

#define DECOMPR_BUF_SIZE    (8192)

/***************************************************************************
 * non necessariamente devono essere potenze di due, questo                *
 * attualmente e` incidentale                                              *
 ***************************************************************************/

static int level_table[] = {
    1280,   // 0
    2304,   // 1
    4352,   // 2
    8448,   // 3
    12400,  // 4
    16640,  // 5 (default)
    32200,  // 6
    48000,  // 7
    65000,  // 8
};

/*
 * dev'essere sincronizzata con la dichiarazione, in lz78.h
 */

static const char* errstrings[] = {
    "nessun errore",
    "dimensione buffer insufficiente",
    "errore leggendo l'input",
    "errore leggendo l'input (a livello di bit)",
    "errore scrivendo l'output",
    "errore scrivendo l'output (a livello di bit)",
    "impossibile allocare memoria",
    "puntatore al file non valido",
    "nucleo LZ78 non correttamente inizalizzato",
    "intestazione del file compresso corrotta o mancante",
};


/***************************************************************************
 * funzioni di supporto                                                    *
 ***************************************************************************/

/**
 * fa il reset un contesto lz78, se esistono le condizioni oppure se e` forzata
 * a farlo.
 * Attualmente (lz78 0.0.x/0.1.x) l'unica condizione per il reset di un
 * contesto lz78 e` l'esaurimento dello spazio libero del suo albero dei
 * simboli.
 * Se la versione del bitstream e` la 2, reinizializza anche correttamente
 * l'albero dei simboli interno.
 *
 * @param ctx       il contesto lz78 da resettare (eventualmente)
 * @param force     forza il reset anche se non ci sarebbe bisogno.
 *
 * @return      TRUE se il reset e` avvenuto, forzato o meno,
 *          FALSE altrimenti
 */
static int
lz78_reset_if_needed(lz78_context_t *ctx, int force) {
    size_t i = 0;
    if(!ctx) {
        return FALSE;
    }

    if(force || symtree_reset_if_no_space(&ctx->tree)) {
#ifdef LZ78_PROFILE
        if(NULL != ctx->stats) {
            ctx->stats->resets++;
        }
        if(ctx->verbose >= 4) {
            symtree_dump(&ctx->tree, stderr);
        }
#endif
        ctx->idx_bits = 1;

        if(ctx->flags & LZ78_BITSTREAM_V2) {
            /**
             * aggiungo i nodi iniziali contenenti tutti
             * i simboli nell'alfabeto
             */
            for(i = 0; i < (1 << BITSIZEOF(sym_t)); i++) {
                if(symtree_add_child(&ctx->tree,
                    SYMTREE_PTR_ROOT, (sym_t)i) >=
                        LZ78_MAX_IDX(ctx)) {
                    ctx->idx_bits++;
                }
            }
        }
        return TRUE;
    }
    return FALSE;
}

#define PUT_FIELD(DST, FIELD) do { \
    size_t w = bit_write((DST), &(FIELD), BITSIZEOF(FIELD)); \
    if(BITSIZEOF(FIELD) != w) { \
        goto hdr_put_err; \
    } \
} while (0)

/**
 * scrive l'header del file compresso.
 *
 * @param ctx       contesto lz78 in base al quale scrivere l'header
 * @param dst       file su cui scrivere l'header
 *
 * @return      0 se l'operazione ha avuto successo
 *          !0 altrimenti
 * @see lz78_get_header
 */
static int
lz78_put_header(lz78_context_t *ctx, BITIO *dst) {
    uint16_t magic = LZ78_MAGIC;
    uint8_t minihdr = 0;
    uint8_t miniflags = 0;
    uint32_t hash = 0;

    if(NULL == ctx) {
        return -1;
    }

    PUT_FIELD(dst, magic);
    /* header */
    minihdr |= ((ctx->flags & 0xf0000000) >> 24);
    minihdr |= (ctx->level & 0x0f);
    PUT_FIELD(dst, minihdr);

    PUT_FIELD(dst, miniflags);

    PUT_FIELD(dst, hash);

    return 0;

hdr_put_err:
    ctx->errnum = LZ78_ERR_BIT_OUTPUT;
    return -1;
}

#define GET_FIELD(SRC, FIELD) do { \
    size_t r = bit_read((SRC), &(FIELD), BITSIZEOF(FIELD)); \
    if(BITSIZEOF(FIELD) != r) { \
        goto hdr_get_err; \
    } \
} while (0)


/**
 * cerca nel file un'intestazione lz78, e se la trova aggiorna
 * il contesto lz78 di conseguenza. Questo puo` sovrascrivere alcuni valori
 * preimpostati nel contesto suddetto; i valori rilevati dal file hanno (in
 * lz78 0.0.x/0.1.x) la precedenza su quelli preimpostati.
 *
 * @param ctx       contesto su cui operare se l'intestazione e` corretta
 * @param src       file in cui cercare, ed eventualmente leggere,
 *          l'intestazione lz78.
 *
 * @see lz78_put_header
 */
static int
lz78_get_header(lz78_context_t *ctx, BITIO *src) {
    uint16_t magic = 0;
    uint8_t minihdr = 0;
    uint8_t miniflags = 0;
    uint32_t hash = 0;

    if(NULL == ctx) {
        return -1;
    }

    GET_FIELD(src, magic);

    if(LZ78_MAGIC != magic) {
        return 1; /* FIXME */
    }

    GET_FIELD(src, minihdr);
    ctx->flags &= 0x0fffffff;
    /* reset 28 MSBits */
    ctx->flags |= ((minihdr & 0xf0) >> 4) << 28;
    ctx->level = (minihdr & 0x0f);

    GET_FIELD(src, miniflags);

    GET_FIELD(src, hash);

    return 0;

hdr_get_err:
    ctx->errnum = LZ78_ERR_BIT_OUTPUT;
    return -1;
}

/***************************************************************************
 * macro di supporto per tutte le funzioni di compressione/decompressione  *
 ***************************************************************************/

#define COMPR_BIT_WRITE(pval, bits) \
    w = bit_write(dst, (pval), (bits));\
    if(w != (bits)) {\
        ctx->errnum = LZ78_ERR_BIT_OUTPUT;\
        error = 1;\
        break;\
    }

#define DECOMPR_BIT_READ(pval, bits) \
    r = bit_read(src, (pval), (bits));\
    if((bits) != r) {\
        ctx->errnum = LZ78_ERR_BIT_INPUT;\
        error = 1;\
        break;\
    }

#define PRINT_PROGRESS(elapsed) \
    if(ctx->verbose >= 1 && 0 == ((elapsed) % PROGRESS_BYTES)) { \
        fprintf(stderr, "elaborati %lu byte...\r", (unsigned long)(elapsed));\
    }

/***************************************************************************
 * compressore/decompressore: prima versione lz78 (subottimale)            *
 ***************************************************************************/

ssize_t
lz78_compress_file_v1(lz78_context_t *ctx, FILE *src, BITIO *dst) {
    ptr_t p = SYMTREE_PTR_ROOT;
    ptr_t p1 = SYMTREE_NULL;
    size_t w = 0;
    ssize_t parsed = 0;
    int error = 0;
    sym_t s;

    while(TRUE) {
        if(lz78_reset_if_needed(ctx, FALSE)) {
            p = SYMTREE_PTR_ROOT;
        }

        fread(&s, sizeof(sym_t), (size_t)1, src);
        if(ferror(src)) {
            error = 1;
            break;
        }

        if(feof(src)) {
            /* puntatore speciale che marca la fine del file */
            ptr_t eof = 0;
            COMPR_BIT_WRITE(&eof, ctx->idx_bits);
            /* ultimo puntatore valido, fuori ordine */
            COMPR_BIT_WRITE(&p, ctx->idx_bits);
            break;
        }

        p1 = symtree_has_child(&ctx->tree, p, s);
        if(SYMTREE_NULL == p1) {
            COMPR_BIT_WRITE(&p, ctx->idx_bits);
#ifdef LZ78_PROFILE
            if(NULL != ctx->stats) {
                ctx->stats->emissions_idx++;
            }
#endif
            COMPR_BIT_WRITE(&s, BITSIZEOF(sym_t));
#ifdef LZ78_PROFILE
            if(NULL != ctx->stats) {
                ctx->stats->emissions_sym++;
            }
#endif
            if(symtree_add_child(&ctx->tree, p, s) >=
                    LZ78_MAX_IDX(ctx)) {
                ctx->idx_bits++;
            }

            p = SYMTREE_PTR_ROOT;
        } else {
            p = p1;
        }
        parsed++;
        PRINT_PROGRESS(parsed);
    }
    if(ctx->verbose) {
        fputs("\n", stderr);
    }
    #ifdef LZ78_PROFILE
    if(NULL != ctx->stats) {
        ctx->stats->last_idx_size = (int)ctx->idx_bits;
    }
    #endif
    if(error) {
        return -1;
    }
    return parsed;
}

ssize_t
lz78_decompress_file_v1(lz78_context_t *ctx, BITIO *src, FILE *dst) {
    ptr_t p = SYMTREE_NULL;
    sym_t c = 0;
    size_t r = 0, path_len = 0;
    size_t parsed = 0;
    /* lunghezza *effettiva* -> quanti simboli validi */
    int error = 0, eof_reached = 0;
    sym_t *dbuf = malloc(sizeof(sym_t) * DECOMPR_BUF_SIZE);
    if(!dbuf) {
        return -1;
    }

    while(TRUE) {
        if(lz78_reset_if_needed(ctx, FALSE)) {
            p = SYMTREE_PTR_ROOT;
        }

        DECOMPR_BIT_READ(&p, ctx->idx_bits);
        if(SYMTREE_NULL == p) {
            eof_reached = 1;
            /*
             * un puntatore a 0 della dimensione attesa
             * indica la fine deil file, seguito dall'ultimo
             * puntatore valido, fuori ordine
             */
            DECOMPR_BIT_READ(&p, ctx->idx_bits);
        } else {
            /*
             * Se non ho rilevato la fine del file,
             * leggo regolarmente il simbolo atteso
             */
            DECOMPR_BIT_READ(&c, BITSIZEOF(sym_t));
        }

        path_len = symtree_get_path_len(&ctx->tree, p);
        if(path_len + 1 >= DECOMPR_BUF_SIZE) {
            /* non dovrebbe succedere mai */
            fprintf(stderr, "(%s) buffer troppo piccolo: %i invece di %lu\n",
                    __FILE__, DECOMPR_BUF_SIZE,
                    (unsigned long)path_len + 1);
            fprintf(stderr, "(%s) ricompilare aumentando il valore di DECOMPR_BUF_SIZE\n",
                    __FILE__);
            break;
        }
        parsed += path_len;
        PRINT_PROGRESS(parsed);

        /*
         * calcolo comunque la distanza dal nodo estratto alla radice,
         * e ricostuisco comunque il percorso di simboli, perche` serve
         * sia nel caso che sia occorsa la fine del file, sia che non sia
         * stata ancora rilevata
         */
        symtree_walk(&ctx->tree, p, dbuf, (size_t)DECOMPR_BUF_SIZE);
        if(!eof_reached) {
            /*
             * emetto l'ultimo carattere solo se non sono ancora arrivato
             * alla fine del file. Il 'carattere' EOF non e` mai maneggiato
             * esplicitamente, non ha senso quindi aggiungerlo all'albero
             * o emetterlo
             */
            dbuf[path_len] = c;
            parsed++;
#ifdef LZ78_PROFILE
            if(NULL != ctx->stats) {
                ctx->stats->emissions_sym++;
            }
#endif
            if(symtree_add_child(&ctx->tree, p, c) >=
                    LZ78_MAX_IDX(ctx)) {
                ctx->idx_bits++;
            }
            fwrite(dbuf, sizeof(sym_t), path_len + 1, dst);
        } else {
            fwrite(dbuf, sizeof(sym_t), path_len, dst);
            break;
        }
    }
    if(ctx->verbose) {
        fputs("\n", stderr);
    }
    fflush(dst);

    free(dbuf);
    #ifdef LZ78_PROFILE
    if(NULL != ctx->stats) {
        ctx->stats->last_idx_size = (int)ctx->idx_bits;
    }
    #endif
    if(error) {
        return -1;
    }
    return (ssize_t)parsed;
}

/***************************************************************************
 * compressore/decompressore: seconda versione lz78                        *
 ***************************************************************************/

/*
 * differisce dalla V1 solo perche` non emette mai i simboli, ma solo gli
 * indici; si preferisce comunque metterla completamente a parte, e non
 * integrarla nella V1 per cercare di rendere piu` indipendenti le due
 * versioni
 */
ssize_t
lz78_compress_file_v2(lz78_context_t *ctx, FILE *src, BITIO *dst) {
    ptr_t p = SYMTREE_PTR_ROOT;
    ptr_t p1 = SYMTREE_NULL;
    size_t w = 0;
    ssize_t parsed = 0;
    int error = 0;
    sym_t s;

    while(TRUE) {
        lz78_reset_if_needed(ctx, FALSE);
        /*
         * non devo reimpostare il puntatore perche`
         * questo avviene implicitamente
         */
        fread(&s, sizeof(sym_t), (size_t)1, src);
        if(ferror(src)) {
            error = 1;
            break;
        }

        if(feof(src)) {
            /* puntatore speciale che marca la fine del file */
            ptr_t eof = 0;
            COMPR_BIT_WRITE(&eof, ctx->idx_bits);
            /* ultimo puntatore valido, fuori ordine */
            COMPR_BIT_WRITE(&p, ctx->idx_bits);
            break;
        }

        p1 = symtree_has_child(&ctx->tree, p, s);
        if(SYMTREE_NULL == p1) {
            COMPR_BIT_WRITE(&p, ctx->idx_bits);
#ifdef LZ78_PROFILE
            if(NULL != ctx->stats) {
                ctx->stats->emissions_idx++;
            }
#endif
            if(symtree_add_child(&ctx->tree, p, s) >=
                    LZ78_MAX_IDX(ctx)) {
                ctx->idx_bits++;
            }

            p = symtree_has_child(&ctx->tree, SYMTREE_PTR_ROOT, s);
        } else {
            p = p1;
        }
        parsed++;
        PRINT_PROGRESS(parsed);
    }
    if(ctx->verbose) {
        fputs("\n", stderr);
    }
    #ifdef LZ78_PROFILE
    if(NULL != ctx->stats) {
        ctx->stats->last_idx_size = (int)ctx->idx_bits;
    }
    #endif
    if(error) {
        return -1;
    }
    return parsed;
}

ssize_t
lz78_decompress_file_v2(lz78_context_t *ctx, BITIO *src, FILE *dst) {
    ptr_t p = SYMTREE_NULL, plast = SYMTREE_NULL, pa = SYMTREE_NULL;
    sym_t s;
    size_t r = 0, path_len = 0;
    size_t parsed = 0;
    /* lunghezza *effettiva* -> quanti simboli validi */
    int error = 0, eof_reached = 0;
    sym_t *dbuf = malloc(sizeof(sym_t) * DECOMPR_BUF_SIZE);
    if(!dbuf) {
        return -1;
    }

    while(TRUE) {
        if(lz78_reset_if_needed(ctx, FALSE)) {
            p = SYMTREE_PTR_ROOT;
        }

        DECOMPR_BIT_READ(&p, ctx->idx_bits);
        if(SYMTREE_NULL == p) {
            eof_reached = 1;
            /*
             * un puntatore a 0 della dimensione attesa
             * indica la fine del file, seguito dall'ultimo
             * puntatore valido, fuori ordine.
             */
            DECOMPR_BIT_READ(&p, ctx->idx_bits);
        }

        path_len = symtree_get_path_len(&ctx->tree, p);
        if(path_len + 1 >= DECOMPR_BUF_SIZE) {
            /* non dovrebbe succedere mai */
            fprintf(stderr, "(%s) buffer troppo piccolo: %i invece di %lu\n",
                    __FILE__, DECOMPR_BUF_SIZE,
                    (unsigned long)path_len + 1);
            fprintf(stderr, "(%s) ricompilare aumentando il valore di DECOMPR_BUF_SIZE\n",
                    __FILE__);
            break;
        }
        parsed += path_len;
        PRINT_PROGRESS(parsed);

        /*
         * calcolo comunque la distanza dal nodo estratto alla radice,
         * e ricostuisco comunque il percorso di simboli, perche` serve
         * sia nel caso che sia occorsa la fine del file, sia che non sia
         * stata ancora rilevata
         */
        if(SYMTREE_PTR_ROOT != plast && SYMTREE_NULL != plast) {
            /*
             * sovrascrivo il simbolo fittizio (c -> 0) inserito
             * all'iterazione precedente.
             * Non puo` verificarsi che pa == p.
             * uguale a 'p' nulla di male
             */
            pa = symtree_find_ancestor(&ctx->tree, p);
            /* in due passi per evitare ogni problema con le macro */
            s = NODE_GET_SYM(&ctx->tree, pa);
            NODE_SET_SYM(&ctx->tree, plast, s);
        }
        symtree_walk(&ctx->tree, p, dbuf, (size_t)DECOMPR_BUF_SIZE);
        /*
         * emetto l'ultimo carattere solo se non sono ancora arrivato
         * alla fine del file. Il 'carattere' EOF non e` mai maneggiato
         * esplicitamente, non ha senso quindi aggiungerlo all'albero
         * o emetterlo
         */
#ifdef LZ78_PROFILE
        if(NULL != ctx->stats) {
            ctx->stats->emissions_sym++;
        }
#endif
        /*
         * aggiungo simbolo fittizio, che sara` sovrascritto all'iterazione
         * seguente
         */
        plast = symtree_add_child(&ctx->tree, p, 0);
        if(plast >= LZ78_MAX_IDX(ctx)) {
            ctx->idx_bits++;
        }
        fwrite(dbuf, sizeof(sym_t), path_len, dst);
        if(eof_reached) {
            break;
        }
    }
    if(ctx->verbose) {
        fputs("\n", stderr);
    }
    fflush(dst);

    free(dbuf);
    #ifdef LZ78_PROFILE
    if(NULL != ctx->stats) {
        ctx->stats->last_idx_size = (int)ctx->idx_bits;
    }
    #endif
    if(error) {
        return -1;
    }
    return (ssize_t)parsed;

}

/***************************************************************************
 * funzioni esportate                                                      *
 ***************************************************************************/

#ifdef LZ78_PROFILE
void
lz78_print_stats(lz78_context_t *ctx, FILE *f) {
    if(!ctx || (!ctx->stats || !f)) {
        return;
    }

    if(ctx->verbose >= 4) {
        symtree_dump(&ctx->tree, stderr);
    }

    fprintf(f, "\nstatistiche per quest'utilizzo di lz78:\n");
    fprintf(f, "simboli : %07i\n",
            ctx->stats->emissions_sym);
    fprintf(f, "indici  : %07i\n",
            ctx->stats->emissions_idx);
    fprintf(f, "reset   : %04i\n", ctx->stats->resets);
    fprintf(f, "ultimo indice su %03i bit\n", ctx->stats->last_idx_size);
    fflush(f);
}
#endif

const char*
lz78_strerror(lz78_context_t *ctx) {
    if(!ctx) {
        return "";
    }
    /* FIXME: bound checking... */
    return errstrings[ctx->errnum];
}

/*
 * raccoglie tutti i test e i controlli preliminari, cosi` come
 * l'inizializzazione e la finalizzazione delle risorse necessarie;
 * in questo modo le funzioni che eseguono la compressione effettiva
 * (compress_file_v*) possono andare 'a botta sicura'.
 */
ssize_t
lz78_compress_file(lz78_context_t *ctx, FILE *src, BITIO *dst) {
#ifdef LZ78_PROFILE
    lz78_stats_t stats;
#endif
    ssize_t ret = -1;
    int err = 0;

    LZ78_COMMON_CHECK;
#ifdef LZ78_PROFILE
    LZ78_STATS_INIT(&stats);
    ctx->stats = &stats;
#endif
    ctx->level = CLAMP(ctx->level, LZ78_LEVEL_MIN, LZ78_LEVEL_MAX);
    err = symtree_init(&ctx->tree, level_table[ctx->level], SYMTREE_DOWNSTREAM);
    if(err) {
        ctx->errnum = LZ78_ERR_NOT_READY;
        return -1;
    }

    if(ctx->verbose >= 2) {
        fprintf(stderr, "[debug] ciclo compressore...\n");
                fprintf(stderr, "[debug] compressione a livello = %i"
                                " | versione bitstream = %i\n",
                            ctx->level + 1,
               (int)LZ78_GET_BITSTREAM_VER(ctx));
    }

    lz78_reset_if_needed(ctx, TRUE);
    /* puo` essere anticipato purche` rimanga sotto il CLAMP() */
    lz78_put_header(ctx, dst);

    if(ctx->flags & LZ78_BITSTREAM_V2) {
        ret = lz78_compress_file_v2(ctx, src, dst);
    } else {
        ret = lz78_compress_file_v1(ctx, src, dst);
    }

    LZ78_COMMON_EPILOGUE;

    symtree_fini(&ctx->tree);
    return ret;
}

/*
 * raccoglie tutti i test e i controlli preliminari, cosi` come
 * l'inizializzazione e la finalizzazione delle risorse necessarie;
 * in questo modo le funzioni che eseguono la decompressione effettiva
 * (decompress_file_v*) possono andare 'a botta sicura'.
 */
ssize_t
lz78_decompress_file(lz78_context_t *ctx, BITIO *src, FILE *dst) {
#ifdef LZ78_PROFILE
    lz78_stats_t stats;
#endif
    int ret = -1;
    int err = 0;

    LZ78_COMMON_CHECK;
#ifdef LZ78_PROFILE
    LZ78_STATS_INIT(&stats);
    ctx->stats = &stats;
#endif
    /*
     * dato che l'header nel file ha la precedenza sulle impostazioni
     * utente, deve essere fatta prima possibile
     */
    err = lz78_get_header(ctx, src);
    if(err) {
        ctx->errnum = LZ78_ERR_BAD_HEADER;
        return -1;
    }

    ctx->level = CLAMP(ctx->level, LZ78_LEVEL_MIN, LZ78_LEVEL_MAX);
    err = symtree_init(&ctx->tree, level_table[ctx->level], SYMTREE_UPSTREAM);
    if(err) {
        ctx->errnum = LZ78_ERR_NOT_READY;
        return -1;
    }

    if(ctx->verbose >= 2) {
        fprintf(stderr, "[debug] ciclo decompressore...\n");
                fprintf(stderr, "[debug] decompressione a livello = %i"
                                " | versione bitstream = %i\n",
                            ctx->level + 1,
                (int)LZ78_GET_BITSTREAM_VER(ctx));
    }

    lz78_reset_if_needed(ctx, TRUE);

    if(ctx->flags & LZ78_BITSTREAM_V2) {
        ret = lz78_decompress_file_v2(ctx, src, dst);
    } else {
        ret = lz78_decompress_file_v1(ctx, src, dst);
    }

    LZ78_COMMON_EPILOGUE;

    symtree_fini(&ctx->tree);
    return ret;
}
/* EOF */
