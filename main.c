/*
 * lzip78 - LZ78 standalone compressor
 * (c) 2005-2013 Francesco Romani <fromani on gmail>
 * License: MIT
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "options.h"
#include "bitio.h"
#include "lz78.h"


enum {
    COMPR_BUF_SIZE = 512,
    DECOMPR_BUF_SIZE = 512
};

/**
 * trivial extension matching check; if passed, we assume (!!!)
 * the file is lz78 compressed (!!).
 * And yes, this is borderline naive.
 */
static int
source_looks_good(const char *file) {
    char *pc = NULL;
    if(!file || !strlen(file)) {
        return FALSE;
    }
    if(!strcmp(file, "stdin") || !strcmp(file, "-")) {
        return TRUE;
    }
    pc = strrchr(file, '.');
    if(!strcmp(pc+1, DEFAULT_EXT)) {
        return TRUE;
    }
    return FALSE;
}

#define RETURN_IF_NO_SOURCE(POPTS) do { \
    if(NULL == (POPTS)->infile) { \
        fprintf(stderr, \
                "(%s) missing input file!\n", \
                __FILE__); \
        return 2; \
    } \
} while (0)

#define WARN_IF_NO_SOURCE(PSRC) do { \
    if(!(PSRC)) { \
        fprintf(stderr, \
                "(%s) unable to open the source file\n", \
                __FILE__); \
    } \
} while (0)

#define WARN_IF_NO_DEST(PDST, NAME) do { \
    if(!(PDST)) { \
        fprintf(stderr, \
                "(%s) unable to open the destination file '%s'\n", \
                __FILE__, (NAME)); \
    } \
} while (0)

#define RETURN_CLOSED(RET, POPTS, FIO, FD, BIO) do { \
    if((POPTS)->verbose >= 2) { \
        fprintf(stderr, "[debug] closing the stream\n"); \
    } \
    if(NULL != (FIO) && !(POPTS)->use_stdio) { \
        fclose(FIO); \
    } \
    if(NULL != (FD) && !(POPTS)->use_stdio) { \
        fclose(FD); \
    } \
    if(NULL != (BIO)) { \
        bit_close(BIO); \
    } \
    return ret; \
} while (0)


static int
compress(options_t *opts) {
    FILE *src = stdin;
    FILE *fd = NULL;
    BITIO *dst = NULL;
    char namebuf[PATH_MAX_LEN] = "stdout";
    int ret = 0;
    lz78_context_t compr;

    LZ78_CONTEXT_INIT(&compr);

    compr.flags |= LZ78_COMPRESS;
    compr.verbose = opts->verbose;
    compr.level = opts->level;
    if(1 == opts->bitstream_ver) {
        compr.flags |= LZ78_BITSTREAM_V1;
    } else {
        compr.flags |= LZ78_BITSTREAM_V2;
    }

    RETURN_IF_NO_SOURCE(opts);

    if(!strcmp(opts->infile, "stdin") || !strcmp(opts->infile, "-")) {
        src = stdin; /* again */
    } else {
        src = fopen(opts->infile, "rb");
        WARN_IF_NO_SOURCE(src);
    }

    if(opts->use_stdio || src == stdin) {
        fd = stdout;
    } else {
        snprintf(namebuf, (size_t)PATH_MAX_LEN, "%s.%s",
                 opts->infile, opts->suffix);
        fd = fopen(namebuf, "wb");
        WARN_IF_NO_DEST(fd, namebuf);
    }

    dst = bit_open_w(fd, (io_write)fwrite, (io_flush)fflush);
    WARN_IF_NO_DEST(dst, namebuf);

    if(NULL != src && NULL != dst) {
        ret = lz78_compress_file(&compr, src, dst);
    }

    RETURN_CLOSED(ret, opts, src, fd, dst);
}

static int
decompress(options_t *opts) {
    BITIO *src = NULL;
    FILE *dst = NULL;
    FILE *fd = NULL;
    char namebuf[PATH_MAX_LEN] = "stdout";
    char *pc = NULL;
    size_t l = 0;
    lz78_context_t decompr;
    int ret = 0;

    LZ78_CONTEXT_INIT(&decompr);

    decompr.flags |= LZ78_DECOMPRESS;
    decompr.verbose = opts->verbose;

    RETURN_IF_NO_SOURCE(opts);

    if(!source_looks_good(opts->infile)) {
        fprintf(stderr,
                "(%s) WARNING: the input doesn't look good\n",
                __FILE__);
    }
    fd = fopen(opts->infile, "rb");
    if (!fd) {
        WARN_IF_NO_SOURCE(fd);
    }
    src = bit_open_r(fd, (io_read)fread);
    if(!src) {
        WARN_IF_NO_SOURCE(src);
    }

    if(opts->use_stdio) {
        dst = stdout;
    } else {
        pc = strstr(opts->infile, "."DEFAULT_EXT);
        if(NULL != pc) {
            l = MIN(pc - opts->infile, PATH_MAX_LEN);
            strncpy(namebuf, opts->infile, l);
            namebuf[l] = '\0';
        } else {
            /* mumble... */
            snprintf(namebuf, (size_t)PATH_MAX_LEN, "%s.%s",
                     opts->infile, "unlz");
        }
        dst = fopen(namebuf, "wb");
        WARN_IF_NO_DEST(dst, namebuf);
    }

    if(NULL != src && NULL != dst) {
        ret = lz78_decompress_file(&decompr, src, dst);
    }

    RETURN_CLOSED(ret, opts, dst, fd, src);
}


int
main(int argc, char *argv[]) {
    options_t opts;
    int ret = 0;

    options_default(&opts);
    ret = options_parse_cmdline(&opts, argc, argv);
    if(ret <= 0) {
        exit(ret);
    }

    if(LZ78_COMPRESS == opts.mode) {
        ret = compress(&opts);
    } else if(LZ78_DECOMPRESS == opts.mode) {
        ret = decompress(&opts);
    } else {
        fprintf(stderr,
                "(%s) ERROR: unknown operation mode\n",
                __FILE__);
        ret = 1;
    }

    return ret;
}

/* EOF */
