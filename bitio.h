/*
 * lzip78 - bit-level I/O interface
 * (c) 2005-2013 Francesco Romani <fromani on gmail>
 * License: MIT
 */

#ifndef LZIP78_BITIO_H
#define LZIP78_BITIO_H

#include <stdint.h>


#define BITIO_VERSION "0.2"


struct _bit_block8_s {
	/** buffer per i bit */
	uint8_t bits;
	
	/**
	 * elementi validi nel buffer, 
	 * a partire dal LSB (a DX) 
	 */
	int used;
};

typedef struct _bit_block8_s bit_block8_t;

typedef size_t (*io_read)(void *ptr, size_t size, size_t nmemb, void *handle);
typedef size_t (*io_write)(const void *ptr, size_t size, size_t nmemb, void *handle);
typedef int (*io_flush)(void *handle);

/** how many NOT valid (NOT significant) bits in a block? */
#define BITBLANKS(bb) (8 - (bb)->used)

struct _bitio_t {
	/** bit accumulator */
	bit_block8_t buf;

    /** flag */
	int readonly;

    /** the real I/O handle */
    void *handle;
    io_read read;
    io_write write;
    io_flush flush;
};

typedef struct _bitio_t BITIO;

/**
 * apre un nuovo descrittore per l'I/O bit a bit
 *
 * @param filename	path del file da aprire in lettura o scrittura
 * @param mode		modalita` di apertura: BITIO_READ per la lettura o
 *			BITIO_WRITE per la scrittura. Le due modalita` sono
 *			mutuamente esclusive
 * @return		un puntatore ad un nuovo descrittore (da liberare con 
 * 			bitio_close()) se la chiamata ha successo, 
 * 			NULL altrimenti
 * @see bit_close
 */
BITIO* bit_open_r(void *handle, io_read read);
BITIO* bit_open_w(void *handle, io_write write, io_flush flush);

/**
 * chiude un descrittore aperto con bitio_open(), liberando le eventuali
 * risorse acquisite e scaricando gli eventuali buffer.
 *
 * @param bio		puntatore al descrittore da chiudere
 * @return		0	se l'operazione ha successo
 * 			-1	se si verificano errori nello 
 * 				scaricamento dei buffer
 * 			-2	se si verificano errori nella 
 * 				chiusura del file
 * @see bit_open
 */
int bit_close(BITIO *bio);

/**
 * cerca di scrivere il quantitativo di BIT indicati, leggendoli dall'area
 * di memoria fornita.
 *
 * @param bio		puntatore al descrittore su cui operare
 * @param data		puntatore ai BIT da scrivere
 * @param len		quanti BIT scrivere
 * @return		il numero di BIT scritti con successo
 * 			<0 in caso di errore
 * @see bit_open
 * @see bit_read
 */
int bit_write(BITIO *bio, const void *data, size_t len);

/**
 * cerca di leggere il quantitativo di BIT indicati, copiandoli nell'area
 * di memoria fornita.
 *
 * @param bio		puntatore al descrittore su cui operare
 * @param data		puntatore alla memoria in cui copiare i BIT letti
 * @param len		quanti BIT leggere
 * @return		il numero di BIT letti con successo
 * 			<0 in caso di errore
 * @see bit_open
 * @see bit_write
 */
int bit_read(BITIO *bio, void *data, size_t len);

#endif // LZIP78_BITIO_H
