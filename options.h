/*
 * lzip78 - option handling interface.
 * (c) 2005-2013 Francesco Romani <fromani on gmail>
 * License: MIT
 */

#ifndef LZIP78_OPTIONS_H
#define LZIP78_OPTIONS_H


#include <stdint.h>


typedef struct _options_t options_t;

struct _options_t {
    /** compressore o decompressore */
    uint32_t mode;

    /** quantita` di messaggi */
    int verbose;

    /**
     *livello di compressione:
     * 1: minore
     * 9: maggiore
     */
    int level;

    /**
     * (de)comprime leggendo/scrivendo
     * su standard input/output
     */
    int use_stdio;

    /**
     * versione del bitstream da usare
     */
    int bitstream_ver;

    /** nome file da comprimere */
    const char *infile;

    /**
     *suffisso del file compresso
     *(se presente)
     */
    const char *suffix;

    int block_size;
};

/* API pubblica */

/**
 * inizializza le opzioni ai valori di default
 *
 * @param opts      opzioni da inizializzare
 * @see options_parse_cmdline
 */
void options_default(options_t *opts);

/**
 * analizza la riga di comando e ricava le opzioni
 *
 * @param opts      memorizza qui le opzioni ricavate
 * @param argc      come per main()
 * @param argv      come per main()
 *
 * @return il numero delle opzioni analizzate con successo,
 *         -1 in caso di errore
 * @see options_default
 */
int options_parse_cmdline(options_t *opts, int argc, char *argv[]);

/**
 * mostra un messaggio di spiegazioni di utilizzo
 *
 * @param program   nome del programma
 */
void options_show_help(const char *program);

/**
 * mostra la versione corrente
 *
 * @param program   nome del programma
 */
void options_show_version(const char *program);

/**
 * mostra la licenza d'uso del programma
 *
 * @param program   nome del programma
 */
void options_show_license(const char *program);

#endif // LZIP78_OPTIONS_H
/* EOF */
