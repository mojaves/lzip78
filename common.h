/*
 * lzip78 - common code, constants, macros and utilities.
 * (c) 2005-2013 Francesco Romani <fromani on gmail>
 * License: MIT
 */

#ifndef LZIP78_COMMON_H
#define LZIP78_COMMON_H

#include <stdio.h>
#include <stdint.h>
#include <unistd.h>

#ifndef TRUE
#define TRUE            (1)
#endif
#ifndef FALSE
#define FALSE           (0)
#endif

#ifndef MIN
#define MIN(a,b)        (((a) < (b)) ?(a) :(b))
#endif

#ifndef MAX
#define MAX(a,b)        (((a) > (b)) ?(a) :(b))
#endif

#ifndef CLAMP
#define CLAMP(x, a, b)      (MAX((a), MIN((b), (x))))
#endif
/** coherce 'x' between 'a' and 'b' */

#ifndef BITSIZEOF
#define BITSIZEOF(t)        (sizeof(t) << 3)
#endif
/** size of 't' in bits instead of bytes */

#define PATH_MAX_LEN        (255)

#define DEFAULT_EXT         "lz"

#define COMPRESSOR_NAME         "lzip78"
#define DECOMPRESSOR_NAME       "lunzip78"

#define LZ78_MAGIC              (0xfeed)

#define PROGRESS_BYTES      (1024)
/* print progress every PROGRESS_BYTES */

enum {
    BLOCK_SIZE_MIN      = 1024,
    BLOCK_SIZE_DEFAULT  = 1024 * 1024,
    BLOCK_SIZE_MAX      = 16 * 1024 * 1024
};

typedef uint8_t sym_t;

#endif // LZIP78_COMMON_H
/* EOF */
