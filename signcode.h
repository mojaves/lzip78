/*
 * lzip78 - signature function(s) interface.
 * (c) 2013 Francesco Romani <fromani on gmail>
 * License: MIT
 */

#ifndef LZIP78_SIGNCODE_H
#define LZIP78_SIGNCODE_H

#include "common.h"

typedef struct signcode SIGNCODE;

SIGNCODE *signcode_new(uint32_t seed);
void signcode_del(SIGNCODE *SC);

/* 
 * adler32 has `seed' for signature consistency, but
 * always ignore that.
 */
uint32_t adler32_sign(const void *key, int len, uint32_t seed);
uint32_t murmur2_hash32(const void *key, int len, uint32_t seed);


SIGNCODE *adler32_new(uint32_t seed);
void adler32_add(SIGNCODE *SC, const uint8_t *data, int len);
uint32_t adler32_digest32(SIGNCODE *SC);
uint32_t adler32_digest32_x(const uint8_t *data, int len, uint32_t seed);

#define murmur2_new signcode_new
void murmur2_add(SIGNCODE *SC, const uint8_t *data, int len);
uint32_t murmur2_digest32(SIGNCODE *SC);
uint32_t murmur2_digest32_x(const uint8_t *data, int len, uint32_t seed);

#endif // LZIP78_SIGNCODE_H
/* EOF */
