/*
 * lzip78 - signature function(s) implementation.
 * (c) 2013 Francesco Romani <fromani on gmail>
 * License: MIT
 */

#include <stdlib.h>

#include "signcode.h"


struct signcode {
	uint32_t hash;
	uint32_t tail;
	uint32_t count;
	uint32_t size;
};


SIGNCODE *
signcode_new(uint32_t seed) {
    SIGNCODE *SC = calloc(1, sizeof(*SC));
    if (SC) {
        SC->hash = seed;
    }
    return SC;
}

void
signcode_del(SIGNCODE *SC) {
    free(SC);
}

/* see http://en.wikipedia.org/wiki/Adler-32 */

enum {
    MOD_ADLER = 65521
};
 
uint32_t
adler32_sign(const void *key, int len, uint32_t seed) {
    const uint8_t *data = key;
    uint32_t a = 1, b = 0;
    int32_t i;
    /* see .h for explanation */
    (void)seed;

    for (i = 0; i < len; i++) {
        a = (a + data[i]) % MOD_ADLER;
        b = (b + a) % MOD_ADLER;
    }
 
    return (b << 16) | a;
}

SIGNCODE *
adler32_new(uint32_t seed) {
    SIGNCODE *SC = signcode_new(seed);
    if (SC) {
        SC->hash = 1;
    }
    return SC;
}

void
adler32_add(SIGNCODE *SC, const uint8_t *data, int len) {
    if (SC) {
        int32_t i;
        for (i = 0; i < len; i++) {
            SC->hash = (SC->hash + data[i]) % MOD_ADLER;
            SC->tail = (SC->tail + SC->hash) % MOD_ADLER;
        }
    }
}

uint32_t
adler32_digest32(SIGNCODE *SC) {
    uint32_t h = 0;
    if (SC) {
        h = (SC->tail << 16) | SC->hash;
    }
    return h;
}

uint32_t
adler32_digest32_x(const uint8_t *data, int len, uint32_t seed) {
    /* see .h for explanation */
    (void)seed;

    SIGNCODE ctx = { 1, 0, 0, 0 };
    adler32_add(&ctx, data, len);
    return adler32_digest32(&ctx);
}



/* code derived from: MurmurHash2A, by Austin Appleby */

#define mmix(h,k) { k *= m; k ^= k >> r; k *= m; h *= m; h ^= k; }

enum {
    M1 = 0x5bd1e995,
    R1 = 24
};

uint32_t
murmur2_hash32(const void *key, int len, uint32_t seed) {
	const uint32_t m = M1;
	const int r = R1;
	uint32_t l = len;
	const uint8_t *data = key;
	uint32_t h = seed;
	uint32_t t = 0;

    /* body blocks */
	while (len >= 4) {
		uint32_t k = *(uint32_t*)data;

		mmix(h,k);

		data += 4;
		len -= 4;
	}

    /* tail */
	switch(len)	{
	case 3: t ^= data[2] << 16;
	case 2: t ^= data[1] << 8;
	case 1: t ^= data[0];
	};

    /* packing */
	mmix(h, t);
	mmix(h, l);

	h ^= h >> 13;
	h *= m;
	h ^= h >> 15;

	return h;
}


static void
mixtail(SIGNCODE *SC,
        const uint8_t *data, int len,
        const uint8_t **outdata, int *outlen) {
    while (len && ((len < 4) || SC->count)) {
        SC->tail |= (*data++) << (SC->count * 8);

        SC->count++;
		len--;

		if(SC->count == 4) {
            uint32_t m = M1;
            uint32_t r = R1;

            mmix(SC->hash, SC->tail);
			SC->tail = 0;
			SC->count = 0;
		}
	}
    if (outdata) *outdata = data;
    if (outlen) *outlen = len;
}

void
murmur2_add(SIGNCODE *SC, const uint8_t *data_, int len_) {
    if (SC) {
        const uint8_t *data = data_;
        int len = len_;

        SC->size += len_;

		mixtail(SC, data_, len_, &data, &len);

		while (len >= 4) {
			uint32_t k = *(uint32_t*)data;
            uint32_t m = M1;
            uint32_t r = R1;

			mmix(SC->hash, k);

			data += 4;
			len -= 4;
		}

        /* we don't need the adjusted outputs. */
		mixtail(SC, data, len, NULL, NULL);
	}
}

uint32_t
murmur2_digest32(SIGNCODE *SC) {
    uint32_t h = 0;
    if (SC) {
        uint32_t m = M1;
        uint32_t r = R1;

        mmix(SC->hash, SC->tail);
		mmix(SC->hash, SC->size);

		SC->hash ^= SC->hash >> 13;
		SC->hash *= M1;
		SC->hash ^= SC->hash >> 15;

        h = SC->hash;
    }
    return h;
}

uint32_t
murmur2_digest32_x(const uint8_t *data, int len, uint32_t seed) {
    SIGNCODE ctx = { seed, 0, 0, 0 };
    murmur2_add(&ctx, data, len);
    return murmur2_digest32(&ctx);
}

/* EOF */
