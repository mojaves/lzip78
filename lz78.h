/*
 * Francesco Romani 20050324 - 09:10
 * compressore/decompressore lz78
 */

#ifndef _LZ78_H_INCLUDED_
#define _LZ78_H_INCLUDED_

#include "common.h"
#include "bitio.h"
#include "symtree.h"

#define LZ78_CORE_VERSION   "0.2.0"

/** per comodita` del chiamante */
#define LZ78_COMPRESS       (0)
#define LZ78_DECOMPRESS     (1)

#define LZ78_LEVEL_MIN      (0)
#define LZ78_LEVEL_DEF      (5)
#define LZ78_LEVEL_MAX      (9)

#define LZ78_FLAGS_DEFAULT  (0)

/** forza l'operazione anche se l'input non pare corretto */
#define LZ78_FORCE      (0x00000002)

/**
 * flags: i 4 MSB sono riservati alla versione del bitstrea,
 */
#define LZ78_BITSTREAM_V1   (0x10000000)
#define LZ78_BITSTREAM_V2   (0x20000000)

/** Macro comode */
#define LZ78_MAX_IDX(ctxp)  (1 << (ctxp)->idx_bits)

#define LZ78_GET_BITSTREAM_VER(ctx) (((ctx)->flags) >> 28)

enum {
    LZ78_ERR_NONE = 0,
    LZ78_ERR_BUF = 1,
    LZ78_ERR_INPUT,
    LZ78_ERR_BIT_INPUT,
    LZ78_ERR_OUTPUT,
    LZ78_ERR_BIT_OUTPUT,
    LZ78_ERR_NOMEM,
    LZ78_ERR_BAD_FILE,
    LZ78_ERR_NOT_READY,
    LZ78_ERR_BAD_HEADER,
};

#ifdef LZ78_PROFILE

typedef struct _lz78_stats_t lz78_stats_t;

/**
 * legenda:
 * "cosa"    -> quale informazione rappresenta il campo
 * "chi"     -> quale metodo aggiorna il campo; se tale metodo
 *              non e` mai invocato, il campo non e` significativo.
 * "perche`" -> qual'e` lo scopo per cui viene memorizzata
 *              questa informazione
 */
struct _lz78_stats_t {
    /**
     * cosa    : chiamate effettuate a lz78_compress()
     * chi     : lz78_compress_file()
     * perche` : per controllare l'efficacia di (DE)COMPR_BUF_SIZE
     */
    int loops;

    /**
     * cosa    : numero di emissioni di simboli
     * chi     : lz78_compress*() e lz78_decompress*()
     * perche` : debug/tuning compressore
     */
    int emissions_sym;

    /**
     * cosa    : numero di emissioni di indici
     * chi     : lz78_compress*()  e lz78_decompress*()
     * perche` : debug/tuning compressore, sopratutto V2
     */
    int emissions_idx;

    /**
     * cosa    : quantita` di bit dell'indice alla chiusura
     * chi     : lz78_compress_file()
     * perche` : debug/tuning compressore
     */
    int last_idx_size;

    /**
     * cosa    : numero di reset effettuati durante la compressione
     * chi     : lz78_compress_file() e lz78_reset_if_needed()
     * perche` : debug/tuning compressore
     */
    int resets;
};

#endif

typedef struct _lz78_context_t lz78_context_t;

struct _lz78_context_t {
    /**
     * livello di compressione:
     * compressore   : impostato dal chiamante
     * decompressore : letto dall'header del file compresso,
     *                 solo se non e` possibile impostato dal
     *                 chiamante
     */
    int level;

    /**
     * Il chiamante dovrebbe impostare solo la versione del
     * bitstream da usare; altri flag per uso interno.
     * compressore   : impostato dal chiamante
     * decompressore : letto dall'header del file compresso,
     *                 solo se non e` possibile impostato dal
     *                 chiamante
     */
    uint32_t flags;

    /**
     * codice dell'ultimo errore occorso. Solo per uso interno.
     */
    int errnum;

    /**
     * bit significativi (usati) dell'indice: 1..32.
     * Solo per uso interno.
     */
    size_t idx_bits;

    /**
     * albero dei simboli per il compressore/decompressore.
     * Solo per uso interno.
     */
    symtree_t tree;
#ifdef LZ78_PROFILE
    /**
     * puntatore alle statistiche di uso. Il chiamante dovrebbe
     * solamente impostare il puntatore ad una struttura valida,
     * e accedervi dopo l'uso mediante print_stats.
     */
    lz78_stats_t *stats;
#endif
    /**
     * quantita` di messaggi da visualizzare.
     * compressore   : impostato dal chiamante
     * decompressore : impostato dal chiamante
     */
    int verbose;
};

/* API pubblica */

/**
 * [MACRO]
 * inizializza un contesto di (de)compressione con valori sicuri.
 * Il chiamante puo` in seguito modificare i valori che ritiene
 * opportuno, tipicamente solo i livelli di compressione e verbosita`
 * richiesti, nonche` la versione del bitstream da usare.
 * Questo in fase di compressione, perche` in fase di decompressione
 * i suddetti valori sono ricavati dai metadati del file compresso,
 * e solo se questi non sono disponibili o contengono valori
 * inattendibili vengono considerate le impostazioni dell'utente.
 *
 * LZ78 0.2.x:
 * l'operazione di decompressione fallisce direttamente se i metadati
 * non sono disponibili.
 */
#define LZ78_CONTEXT_INIT(ctx) \
    do { \
        (ctx)->level = LZ78_LEVEL_DEF; \
        (ctx)->flags = LZ78_FLAGS_DEFAULT; \
        (ctx)->errnum = LZ78_ERR_NONE; \
        (ctx)->idx_bits = 1; \
        (ctx)->verbose = 0; \
    } while(0)

#ifdef LZ78_PROFILE

/**
 * [MACRO]
 * inizializza una struttura contenente le statistiche d'uso
 * di un descrittore lz78.
 * In alcune circostanze puo` essere utile avere piu` informazioni
 * su come ha operato internamente un descrittore lz78. In questi
 * casi, puo` essere utilizzato una struttura di questo tipo
 * per raccogliere queste informazioni.
 * Attualmente le informazioni fornite sono relativamente poche e
 * di alto livello, ma dovrebbero essere sufficienti per coprire
 * i casi piu` comuni. In futuro potranno essere raccolte piu`
 * informazioni. Vedere la struttura lz78_stats_t per una descrizione
 * piu` dettagliata di queste informazioni
 *
 * @param lzst      struttura da inizializzare
 * @see lz78_stats_t
 */
#define LZ78_STATS_INIT(lzst) \
    do { \
        (lzst)->loops = 0;\
        (lzst)->emissions_sym = 0;\
        (lzst)->emissions_idx = 0;\
        (lzst)->last_idx_size = 0;\
        (lzst)->resets = 0;\
    } while(0)


/**
 * scrive sul file indicato le statistiche disponibili sull'ultimo
 * utilizzo del descrittore di (de)compressione
 *
 * @param ctx       descrittore di cui visualizzare le statistiche
 * @param f     file su cui scrivere le statistiche
 * @see LZ78_STATS_INIT
 * @see LZ78_SET_STATS
 */
void lz78_print_stats(lz78_context_t *ctx, FILE *f);

#endif

/**
 * fornisce una stringa descrittiva dell'ultimo errore avvenuto
 *
 * @return un puntatore ad una stringa costante che descrive l'errore
 */
const char* lz78_strerror(lz78_context_t *ctx);

/**
 * Comprime un'intero file secondo l'algoritmo LZ78, leggendo
 * i simboli grezzi dal file sorgente ed emettendo i simboli compressi
 * sul file di destinazione.
 *
 * @param ctx       puntatore al descrittore su cui operare
 * @param src       puntatore al FILE da leggere.
 * @param dst       puntatore al descrittore BITIO a sua volta
 *                      collegato al file da scrivere.
 *
 * @return >0 la dimensione della porzione di file compressa
 *         -1 se e` avvenuto un errore (tipicamente di emissione di bit)
 */
ssize_t lz78_compress_file(lz78_context_t *ctx, FILE *src, BITIO *dst);

/**
 * Decomprime un'intero file secondo l'algoritmo LZ78, leggendo
 * i simboli compressi dal file sorgente ed emettendo i simboli grezzi
 * sul file di destinazione.
 *
 * @param ctx       puntatore al descrittore su cui operare
 * @param src       puntatore al descrittore BITIO a sua volta
 *                      collegato al file da leggere.
 * @param dst       puntatore al FILE da scrivere.
 *
 * @return >0 la dimensione della porzione di file decompressa
 *         -1 se e` avvenuto un errore (tipicamente di emissione di bit)
 */
ssize_t lz78_decompress_file(lz78_context_t *ctx, BITIO *src, FILE *dst);

#endif // _LZ78_H_INCLUDED_
// EOF
