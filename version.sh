#!/bin/sh

VERSION=$( grep "versione corrente" README | cut -d\: -f2 | cut -d\( -f 1 | tr -d [:blank:] )
VERDATE=$( grep "versione corrente" README | cut -d\( -f2 | cut -d\) -f 1 )

:> configure.mk
echo "VERSION=\\\"$VERSION\\\"" >> configure.mk
echo "VERDATE=\"\\\"$VERDATE\\\"\"" >> configure.mk

echo $VERSION
