#
# Francesco Romani - 20050307 - 17:48
# LZ78 de/compressor
#

include configure.mk

CC=gcc
#DEFINES=-DLZ78_PROFILE=1 -DVERSION=$(VERSION) -DVERDATE=$(VERDATE)
#CFLAGS=-O -g -pg -W -Wall -Werror -Wconversion -Wshadow -Wcast-align -DLZ78_PROFILE=1 -DVERSION=$(VERSION) -fPIC -DPIC
CFLAGS=-O -g -pg -W -Wall -Wshadow -Wcast-align -DLZ78_PROFILE=1 -DVERSION=$(VERSION) -fPIC -DPIC
#CFLAGS=-O -g -W -Wall -Werror -Wconversion -Wshadow -Wcast-align $(DEFINES) -fPIC -DPIC

OBJECTS=bitio.o symtree.o lz78.o options.o signcode.o

PROGS=lzip78 lunzip78

all: $(PROGS)

# alias

lib: liblz78.a

shlib: liblz78.so

# target

dist: lzip78
	ln -s lzip78 lunzip78

lzip78: main.c liblz78.a
	$(CC) $(CFLAGS) -o $@ main.c liblz78.a

lunzip78: lzip78
	ln -s lzip78 lunzip78

shlzip78: main.c liblz78.so.0
	$(CC) $(CFLAGS) -o $@ main.c -L. -llz78

liblz78.a: $(OBJECTS)
	ar cru $@ $(OBJECTS)
	ranlib $@

liblz78.so: $(OBJECTS)
	gcc -shared -o $@ $(OBJECTS)
	ln -s $@ $@.0.0.1
	ln -s $@ $@.0

# utilities

release:
	tar zcv -C .. -f ../lz78-`./version.sh`.tgz lz78

clean:
	rm -f $(OBJECTS) liblz78.a $(PROGS) liblz78.so* core.*
